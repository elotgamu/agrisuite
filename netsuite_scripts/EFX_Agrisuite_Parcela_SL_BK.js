/*******************************************************************
* @NApiVersion 2.x
* @NScriptType Suitelet
* Name: EFX_Agrisuite_Parcela_SL.js
*
* Author: Efficientix Dev Team
* Purpose: All stuuf related parcel custom record (CRUD)
* Script: The script record id
* Deploy: The script deployment record id
* ******************************************************************* */

define(['N/record', 'N/search', 'N/ui/serverWidget'], function (record, search, ui) {

  function onRequest(context) {
    var section = '';

    try {

        if (context.request.method == 'GET') {

            section = 'Get Parameters';
            {
                //view
                //create
                //update
                //delete
                var mode = context.request.parameters.custparam_mode || '';
                var terreno = context.request.parameters.custparam_terreno || '';

                var id = context.request.parameters.custparam_id || '';
                var parcela_name = context.request.parameters.custparam_name || '';
                var parcela_size = context.request.parameters.custparam_size || '';
                var measure_unit = context.request.parameters.custparam_unit || '';
                log.audit({ title: 'Parametros:', details: 'Mode: ' + mode + ' | Id: ' + id + ' | Terreno: ' + terreno + ' | Unidad de Medida: ' + measure_unit + ' | nombre pacela: ' + parcela_name});

                var responseResult = {
                    statusCode: '',
                    message: '',
                    id: '',
                    name: '',
                    results: {}
                };
            }

            if (mode == 'view') {

                // var terrenoObj = {};
                // section = 'Get Terrenos';
                // {
                //     var result = search.create({
                //         type: 'customrecord_efx_as_terreno',
                //         filters: [
                //             ['isinactive', search.Operator.IS, 'F']
                //         ],
                //         columns: [
                //             { name: 'name' }
                //         ]
                //     });
                //
                //     var resultData = result.run();
                //     var start = 0;
                //
                //     do {
                //         var resultSet = resultData.getRange(start, start + 1000);
                //         if (resultSet && resultSet.length > 0) {
                //             for (var i = 0; i < resultSet.length; i++) {
                //                 var rId = resultSet[i].id;
                //                 var rName = resultSet[i].getValue({ name: 'name' }) || '';
                //
                //                 if (!terrenoObj[rId]) {
                //                     terrenoObj[rId] = {
                //                         name: rName
                //                     };
                //                 }
                //             }
                //         }
                //         start += 1000;
                //
                //     } while (resultSet && resultSet.length == 1000)
                //
                //     responseResult.statusCode = 2;
                //     responseResult.message = 'Lista Obtenida Exitosamente!';
                //     responseResult.results = terrenoObj;
                // }
            }
            else if (mode == 'create') {
                if (parcela_name) {
                    section = 'Create Parcela';
                    {
                        var rec = record.create({
                            type: 'customrecord_efx_as_unidad'
                        });

                        rec.setValue({
                            fieldId: 'name',
                            value: parcela_name
                        });

                        rec.setValue({
                          fieldId: 'custrecord_efx_as_p_tamanoparcela',
                          value: parcela_size
                        });

                        rec.setValue({
                          fieldId: 'custrecord_efx_as_p_sesion',
                          value: measure_unit
                        });

                        rec.setValue({
                          fieldId: 'custrecord_efx_as_p_terreno',
                          value: terreno
                        });

                        var recId = rec.save({
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        });

                        responseResult.statusCode = 2;
                        responseResult.message = 'Creado Exitosamente!';
                        responseResult.id = recId;
                        responseResult.name = parcela_name;

                        // var terrain_list = get_all_terrains();
                    }
                }
                else {
                    responseResult.statusCode = 3;
                    responseResult.message = 'Error, Es necesario el parametro Terreno!';
                }
            }
            else if (mode == 'detailview') {
              // if (id) {
              //   var terreno_obtenido = get_terrain_details(id);
              //   if (terreno_obtenido == []) {
              //     responseResult.statusCode = 3;
              //     responseResult.message = 'Error, NO se pudo obtener el terreno';
              //   } else {
              //     responseResult.statusCode = 2;
              //     responseResult.message = 'Se ha obtenido el terreno solicitado';
              //   }
              //
              // } else {
              //   responseResult.statusCode = 3;
              //   responseResult.message = 'Error, Es necesario el parametro id del  Terreno!';
              // }
            }

            // section = 'Create Form';
            // {
            //   var form = ui.createForm({
            //     title: ' ',
            //     hideNavBar: true
            //   });
            //
            //   var html = form.addField({
            //     id: 'custpage_html',
            //     type: ui.FieldType.INLINEHTML,
            //     label: 'HTML Content'
            //   });
            //
            //   if (mode == 'create') {
            //     html.defaultValue = JSON.stringify(responseResult) + '<div id="parcelas-create-result" name="selected_frame" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" vspace="0" hspace="0" style="overflow:scroll"></div> <script type="text/javascript">JSON.parse(document.getElementById(\'parcelas-create-result\').innerHTML);</script>';
            //   } else if (mode == 'detailview') {
            //     html.defaultValue = JSON.stringify(responseResult) + '<div id="terrain_details_result" name="selected_frame" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" vspace="0" hspace="0" style="overflow:scroll">'+ JSON.stringify(terreno_obtenido) + '</div>  <script type="text/javascript">parent.App.terrain_selected = JSON.parse(document.getElementById(\'terrain_details_result\').innerHTML);parent.App.update_terrain_data();</script>';
            //   } else {
            //       html.defaultValue = JSON.stringify(responseResult)
            //   }
            //
            // }

            section = 'Print form';
            {
                context.response.write(JSON.stringify(responseResult));
                // context.response.writePage(form);
            }

        }

    } catch (err) {
        throw err;
    }
  }

  return {
    onRequest: onRequest
  };
});
