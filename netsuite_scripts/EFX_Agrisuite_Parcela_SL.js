/*******************************************************************
* @NApiVersion 2.x
* @NScriptType Suitelet
* Name: EFX_Agrisuite_Parcela_SL.js
*
* Author: Efficientix Dev Team
* Purpose: All stuuf related parcel custom record (CRUD)
* Script: The script record id
* Deploy: The script deployment record id
* ******************************************************************* */

define(['N/record', 'N/search', 'N/ui/serverWidget'], function (record, search, ui) {

    function onRequest(context) {
      var section = '';

      try {

          if (context.request.method == 'GET') {

              section = 'Get Parameters';
              {
                  //view
                  //create
                  //update
                  //delete
                  var mode = context.request.parameters.custparam_mode || '';
                  var terreno = context.request.parameters.custparam_terreno || '';

                  var id = context.request.parameters.custparam_id || '';
                  var parcela_name = context.request.parameters.custparam_name || '';
                  var parcela_size = context.request.parameters.custparam_size || '';
                  var measure_unit = context.request.parameters.custparam_unit || '';
                  var unidad_parcela = context.request.parameters.custparam_parcel_unit || '';
                  log.audit({ title: 'Parametros:', details: 'Mode: ' + mode + ' | Id: ' + id + ' | Terreno: ' + terreno + ' | Unidad de Medida: ' + measure_unit + ' | nombre pacela: ' + parcela_name});

                  var responseResult = {
                      statusCode: '',
                      message: '',
                      id: '',
                      name: '',
                      results: {}
                  };
              }
              switch(mode){
                case 'view':
                    var terreno_obtenido = obtenerTerrenos();
                    if (Object.keys(terreno_obtenido).length == 0) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo obtener las parcelas en las unidades en los terreno';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha obtenido las parcelas en las parcelas en los terrenos';
                        responseResult.results = terreno_obtenido;
                    }
                break;
                case 'unit':
                    var unidades_obtenidas = obtenerUnidades();
                    if (Object.keys(unidades_obtenidas).length == 0) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo obtener las unidades';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha obtenido las unidades';
                        responseResult.results = unidades_obtenidas;
                    }
                break;
                case 'create':
                    if (parcela_name) {
                        section = 'Create Parcela';
                        {
                            var rec = record.create({
                                type: 'customrecord_efx_as_parcela'
                            });

                            rec.setValue({
                                fieldId: 'name',
                                value: parcela_name
                            });

                            rec.setValue({
                            fieldId: 'custrecord_efx_as_p_tamanoparcela',
                            value: parcela_size
                            });

                            rec.setValue({
                            fieldId: 'custrecord_efx_as_p_unidad_medida',
                            value: measure_unit
                            });

                            rec.setValue({
                              fieldId: 'custrecord_efx_as_p_unidad',
                              value: unidad_parcela
                            });

                            rec.setValue({
                            fieldId: 'custrecord_efx_as_p_terreno',
                            value: terreno
                            });

                            var recId = rec.save({
                                enableSourcing: true,
                                ignoreMandatoryFields: true
                            });

                            responseResult.statusCode = 2;
                            responseResult.message = 'Creado Exitosamente!';
                            responseResult.id = recId;
                            responseResult.name = parcela_name;
                        }
                    }
                    else {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, Es necesario el parametro Terreno!';
                    }
                break;
                case 'detailview':
                    var tunel_obtenidos = obtenerTuneles(id);
                    if (Object.keys(tunel_obtenidos).length == 0) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo obtener los tuneles de las parcelas';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha obtenido los tuneles de las parcelas';
                        responseResult.results = tunel_obtenidos;
                    }
                break;
                default:
                    responseResult.statusCode = 3;
                    responseResult.message = 'No se ha mandado una accion';
                    responseResult.results = {};
              };

              section = 'Print form';
              {
                  context.response.write(JSON.stringify(responseResult));
              }

          }

      } catch (err) {
          throw err;
      }
    }

    function obtenerUnidades(){
        var unidades = {};
        var result = search.create({
            type: 'customrecord_efx_as_unidad',
            filters: [
            ['isinactive', search.Operator.IS, 'F']
            ],
            columns: [
            {name:'name'},
            {name :'custrecord_efx_as_u_color'}
            ]
        });

        var resultData = result.run();
        var start = 0;
        do {
            var resultSet = resultData.getRange(start, start + 1000);
            if (resultSet && resultSet.length > 0) {
            for (var i = 0; i < resultSet.length; i++) {
                var idUnidad=resultSet[i].id;
                var name =resultSet[i].getValue({name:'name'});
                var color =resultSet[i].getValue({ name :'custrecord_efx_as_u_color' });
                if(!unidades[idUnidad]){
                    unidades[idUnidad]={
                        id:idUnidad,
                        unidadName:name,
                        unidadColor:color
                    };
                }
            }
            }
            start += 1000;

        } while (resultSet && resultSet.length == 1000);

        log.audit({ title: 'unidades', details: JSON.stringify(unidades) });
        return unidades;
    }

    function obtenerTerrenos(){
        var terrenoUnidad = {};

                var result = search.create({
                  type: 'customrecord_efx_as_parcela',
                  filters: [
                    ['isinactive', search.Operator.IS, 'F'], 'and',
                    ['custrecord_efx_as_p_terreno', search.Operator.NONEOF, '@NONE@'],'and',
                    ['custrecord_efx_as_p_unidad', search.Operator.NONEOF, '@NONE@']
                  ],
                  columns: [
                    {name:'name'},
                    {name:'custrecord_efx_as_p_terreno'},
                    { name: 'custrecord_efx_as_p_tamanoparcela' },
                    { name: 'custrecord_efx_as_p_unidad' },
                    { join: 'custrecord_efx_as_p_unidad', name :'custrecord_efx_as_u_color' },
                    { name: 'custrecord_efx_as_p_unidad_medida' }
                  ]
                });

                var resultData = result.run();
                var start = 0;
                do {
                  var resultSet = resultData.getRange(start, start + 1000);
                  if (resultSet && resultSet.length > 0) {
                    for (var i = 0; i < resultSet.length; i++) {
                      var idTerreno=resultSet[i].getValue({ name: 'custrecord_efx_as_p_terreno' });
                      log.audit({ title: 'unidadColor', details: unidadColor });

                      var textoTerreno=resultSet[i].getText({ name: 'custrecord_efx_as_p_terreno' });
                      log.audit({ title: 'unidadName', details: unidadName });


                      var pId = resultSet[i].id;
                      log.audit({ title: 'pId', details: pId });
                      var pName = resultSet[i].getValue({ name: 'name' }) || '';
                      log.audit({ title: 'pName', details: pName });

                      var unidad=resultSet[i].getValue({ name: 'custrecord_efx_as_p_unidad' });
                      log.audit({ title: 'unidad', details: unidad });

                      var unidadColor=resultSet[i].getValue({ join: 'custrecord_efx_as_p_unidad' , name : 'custrecord_efx_as_u_color'});
                      log.audit({ title: 'unidadColor', details: unidadColor });

                      var unidadName=resultSet[i].getText({ name: 'custrecord_efx_as_p_unidad' });
                      log.audit({ title: 'unidadName', details: unidadName });

                      var tamanoparcela=resultSet[i].getValue({ name: 'custrecord_efx_as_p_tamanoparcela' });
                      log.audit({ title: 'tamanoparcela', details: tamanoparcela });

                      var unidad_medida=resultSet[i].getValue({ name: 'custrecord_efx_as_p_unidad_medida' });
                      log.audit({ title: 'unidad_medida', details: unidad_medida });

                      var unidad_medidaName=resultSet[i].getText({ name: 'custrecord_efx_as_p_unidad_medida' });
                      log.audit({ title: 'unidad_medidaName', details: unidad_medidaName });

                      log.audit({ title: 'Terrain details', details: JSON.stringify(terrenoUnidad) });


                      if(!terrenoUnidad[idTerreno]){
                        terrenoUnidad[idTerreno]={
                            id:idTerreno,
                            terrenoName:textoTerreno,
                            unidades:{}
                        };

                      }
                      if (!terrenoUnidad[idTerreno].unidades[unidad]) {
                        terrenoUnidad[idTerreno].unidades[unidad]={
                            id:unidad,
                            unidadName:unidadName,
                            unidadColor:unidadColor,
                            parcelas : {}
                        };
                      }

                      if(!terrenoUnidad[idTerreno].unidades[unidad].parcelas[pId]){
                        terrenoUnidad[idTerreno].unidades[unidad].parcelas[pId] = {
                          id:pId,
                          parcelaName : pName,
                          tamanoparcela : tamanoparcela,
                          unidad_medida : unidad_medida,
                          unidad_medidaName:unidad_medidaName
                        };
                      }

                    }
                  }

                  start += 1000;

                } while (resultSet && resultSet.length == 1000);

                log.audit({ title: 'terrenoUnidad', details: JSON.stringify(terrenoUnidad) });
                return terrenoUnidad;
    }
    function obtenerTuneles(idparcela){
        var objTunel = {};
        var result = search.create({
            type: 'customrecord_efx_as_tunel',
            filters: [
                ['isinactive', search.Operator.IS, 'F'], 'and',
                ['custrecord_efx_as_t_parcela', search.Operator.ANYOF, idparcela]
            ],
            columns: [
                {name:'name'},
                // {name:'custrecord_efx_as_t_parcela'},
                {name:'custrecord_efx_as_t_tamano' },
                {name:'custrecord_efx_as_t_unidad_med' }
            ]
        });

        var resultData = result.run();
        var start = 0;
        do {
            var resultSet = resultData.getRange(start, start + 1000);
            if (resultSet && resultSet.length > 0) {
                for (var i = 0; i < resultSet.length; i++) {
                    var idTunel=resultSet[i].id;
                    var name=resultSet[i].getValue({name:'name'});
                    // var parcela=resultSet[i].getValue({name:'custrecord_efx_as_t_parcela'});
                    var tamano=resultSet[i].getValue({name:'custrecord_efx_as_t_tamano' });
                    var unidad_med=resultSet[i].getValue({name:'custrecord_efx_as_t_unidad_med' });


                    if(!objTunel[idTunel]){
                        objTunel[idTunel]={
                            id:idTunel,
                            name:name,
                            // parcela:parcela,
                            tamano:tamano,
                            unidad_med:unidad_med
                        };
                    }

                }
            }
            start += 1000;
        } while (resultSet && resultSet.length == 1000);
        log.audit({ title: 'objTunel', details: JSON.stringify(objTunel) });
        return objTunel;
    }

    return {
      onRequest: onRequest
    };
  });
