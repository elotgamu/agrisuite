/*******************************************************************
* @NApiVersion 2.x
* @NScriptType Suitelet
* Name: EFX_KDI_INDEX.js
*
* Author: Efficientix Dev Team
* Purpose: A description of the purpose of this script
* Script: The script record id
* Deploy: The script deployment record id
* ******************************************************************* */

define(['N/runtime', 'N/log', 'N/search', 'N/config', 'N/url', 'N/ui/serverWidget', 'N/https'], function (runtime, log, search, config, urlMod, ui, https) {

  function main(context) {

    var section = '';
    var scriptObj = runtime.getCurrentScript();
    var userObj = runtime.getCurrentUser();
    log.audit({ title: 'User', details: JSON.stringify(userObj) });

    try {

      section = 'Get Terrains';
      {
        var terrenoObj = {};

        // var scheme = 'https://';
        // var host = urlMod.resolveDomain({
        //   hostType: urlMod.HostType.APPLICATION
        // });

        var urlTerrain = urlMod.resolveScript({
          scriptId: 'customscript_efx_as_terreno_sl',
          deploymentId: 'customdeploy_efx_as_terreno_sl',
          returnExternalUrl: true
        });

        // urlTerrain = scheme + host + urlTerrain + '&custparam_mode=view';
        urlTerrain = urlTerrain + '&custparam_mode=view';

        log.audit({ title: 'urlTerrain', details: JSON.stringify(urlTerrain) });

        var responseTerrain = https.get({
          url: urlTerrain
        });

        log.audit({ title: 'responseTerrain', details: JSON.stringify(responseTerrain) });
        log.audit({ title: 'responseTerrain Code', details: JSON.stringify(responseTerrain.code) });
        log.audit({ title: 'responseTerrain Body', details: JSON.stringify(responseTerrain.body) });

        if (responseTerrain.code == 200) {
          if (responseTerrain.body) {
            var bodyObj = JSON.parse(responseTerrain.body);
            log.audit({ title: 'BodyObj', details: JSON.stringify(bodyObj) });
            log.audit({ title: 'responseTerrain Message', details: bodyObj.message });
            terrenoObj = bodyObj.results;
          }
        }

        log.audit({ title: 'Body', details: JSON.stringify(terrenoObj) });

      }

      // section = 'Get terrains';
      // {

      //   var result = search.create({
      //     type: 'customrecord_efx_as_terreno',
      //     filters: [
      //       ['isinactive', search.Operator.IS, 'F']
      //     ],
      //     columns: [
      //       { name: 'name' }
      //     ]
      //   });

      //   var resultData = result.run();
      //   var start = 0;

      //   do {
      //     var resultSet = resultData.getRange(start, start + 1000);
      //     if (resultSet && resultSet.length > 0) {
      //       for (var i = 0; i < resultSet.length; i++) {
      //         var rId = resultSet[i].id;
      //         var rName = resultSet[i].getValue({ name: 'name' }) || '';

      //         if (!terrenoObj[rId]) {
      //           terrenoObj[rId] = {
      //             id: rId,
      //             name: rName
      //           };
      //         }
      //       }
      //     }
      //     start += 1000;

      //   } while (resultSet && resultSet.length == 1000);

      //   log.audit({ title: 'Lista de Terrenos', details: JSON.stringify(terrenoObj) })
      // }

      //section get unit measures
      section = 'Get Unidad de Medidas';
      {
        var measure_units = {};
        var u_search = search.create({
          type: 'customrecord_efx_as_unidad_medida',
          filters: [
            ['isinactive', search.Operator.IS, 'F']
          ],
          columns: [
            { name: 'name' }
          ]
        });

        var results = u_search.run();
        var start = 0;

        do {
          var resultSet = results.getRange(start, start + 1000);
          if (resultSet && resultSet.length > 0) {
            for (var i = 0; i < resultSet.length; i++) {
              var uId = resultSet[i].id;
              var uName = resultSet[i].getValue({ name: 'name' }) || '';

              if (!measure_units[uId]) {
                measure_units[uId] = {
                  id: uId,
                  name: uName
                };
              }
            }
          }
          start += 1000;

        } while (resultSet && resultSet.length == 1000);
        log.audit({ title: 'Unidades de medidad', details: JSON.stringify(measure_units) });
      }
      //end get unidades de medidas

      section = 'Create UI';
      {
        var form = ui.createForm({
          title: ' ',
          hideNavBar: false
        });

        var src = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';

        src += 'var rec = currentRecord.get();';
        src += 'debugger;';


        src += 'var frame_url = urlMod.resolveScript({';
        src += 'scriptId: \'customscript_efx_as_terreno_sl\',';
        src += 'deploymentId: \'customdeploy_efx_as_terreno_sl\',';
        src += 'returnExternalUrl: false';
        src += '});';

        // src += 'alert(frame_url);';
        // src += 'var frame_url = \'\';';


        src += 'frame_url += frame_url + \'&mode=create&terreno=\';';
        // src += 'frame_url += \'&rnd=\' + Math.floor(Math.random() * 1000000);';

        src += 'parent.document.getElementById(\'terrain_frame\').src = frame_url;';
        src += 'debugger;';
        src += '});';

        // src += 'alert();'

        // var htmlButtons = '<table style="margin-top:20px;">';
        //
        // htmlButtons += '<tbody>';
        // htmlButtons += '<tr>';
        // htmlButtons += '<td>';
        // htmlButtons += '<input type="button" id="btn_submit" value="Generar" class="" style="padding:7px 14px; font-weight: bold; background-color: #777; color: white;" onclick="' + src + '">';
        //
        // htmlButtons += '</td>';
        // htmlButtons += '</tr>';
        // htmlButtons += '</tbody>';
        // htmlButtons += '</table>';
        //
        // var fieldSubmit = form.addField({
        //   id: 'custpage_htmlbuttons',
        //   label: 'Button',
        //   type: ui.FieldType.INLINEHTML
        // });
        //
        // fieldSubmit.updateLayoutType({
        //   layoutType: ui.FieldLayoutType.STARTROW
        // });
        //
        // fieldSubmit.defaultValue = htmlButtons;

        var html = form.addField({
          id: 'custpage_html',
          type: ui.FieldType.INLINEHTML,
          label: 'HTML Content'
        });

        html.defaultValue = get_template(terrenoObj, measure_units);

        var fieldFrame = form.addField({
          id: 'custpage_selectedframe',
          label: 'Frame',
          type: ui.FieldType.INLINEHTML
        });

        fieldFrame.updateLayoutType({
          layoutType: ui.FieldLayoutType.OUTSIDEBELOW
        });

        fieldFrame.defaultValue = '<iframe id="terrain_frame" name="selected_frame" src="' + '  ' + '" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:scroll"></iframe>';

        context.response.writePage(form);

      }
      //end section create ui

    } catch (e) {
      throw e;
    }
  }

  function get_template(terrains, measure_units) {
    var html = '';
    html += '<!doctype html>';
    html += '<html class="no-js" lang="es">';
    html += '  <head>';
    html += '    <meta charset="utf-8">';
    html += '    <meta name="description" content="KDI Agrisuite">';
    html += '    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
    html += '    <title>KDI-AgriSuite<\/title>';
    html += '';
    html += '    ';
    html += '    <!-- Place favicon.ico in the root directory -->';
    html += '';
    html += '    <!-- build:css styles\/vendor.css -->';
    html += '    <!-- bower:css -->';
    // html += '    <link rel="stylesheet" href="\/bower_components\/components-font-awesome\/css\/font-awesome.css" \/>';
    html += '    <!-- endbower -->';
    html += '    <!-- endbuild -->';
    html += '    <link rel="stylesheet" href="https:\/\/stackpath.bootstrapcdn.com\/bootstrap\/4.1.0\/css\/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">';
    html += '';
    html += '';
    html += '    <!-- build:css styles\/main.css -->';
    html += '    <link rel="stylesheet" href="https://system.netsuite.com/core/media/media.nl?id=3430&c=TSTDRV905028&h=fc41008a6daa69ac89bd&_xt=.css">';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <!-- build:js scripts\/vendor\/modernizr.js -->';
    // html += '    <script src="\/bower_components\/modernizr\/modernizr.js"><\/script>';
    html += '    <!-- endbuild -->';
    html += '  <\/head>';
    html += '  <body>';
    html += '    <!--[if IE]>';
    html += '      <p class="browserupgrade">You are using an <strong>outdated<\/strong> browser. Please <a href="http:\/\/browsehappy.com\/">upgrade your browser<\/a> to improve your experience.<\/p>';
    html += '    <![endif]-->';
    html += '';
    html += '    <div class="container">';
    html += '      <div class="header">';
    html += '        <!-- <ul class="nav nav-pills float-right">';
    html += '          <li class="nav-item">';
    html += '            <a class="nav-link active" href="#">Home<\/a>';
    html += '          <\/li>';
    html += '          <li>';
    html += '            <a class="nav-link" href="#">About<\/a>';
    html += '          <\/li>';
    html += '          <li>';
    html += '            <a class="nav-link" href="#">Contact<\/a>';
    html += '          <\/li>';
    html += '        <\/ul> -->';
    html += '        <h3 class="text-muted">AgriSuite<\/h3>';
    html += '      <\/div>';
    html += '';
    html += '    <\/div>';
    html += '';
    html += '';


    // THE HTML managed by the VUE instance
    html += '<section class="vue-area" id="farm-vue-area">';
    html += '      <router-view :terrain_list="terrains"></router-view>';
    // html += '      <div class="container">';
    // html += '        <div class="row farm-area mb-5">';
    // for (var terrain in terrains) {
    //   html += '          <section data-terrain="terrain" class="col-sm-6 terrain">';
    //   html += '            <div class="terrain-title text-center">';
    //   html += '              <p>' + terrains[terrain].name + ' </p>';
    //   html += '            </div>';
    //   html += '';
    //   html += '            <div class="terrain-body">';
    //   html += '              <p class="text-center">';
    //   html += '                <a class="link" href="#">Ver Terreno</a>';
    //   html += '              </p>';
    //   html += '            </div>';
    //   html += '          </section>';
    // }
    // html += '        </div>';
    // html += '        </div>';
    html += '<div class="container">';
    html += '  <div class="row">';
    html += '    <textarea class="col-sm-12" id="response_result" v-on:change="get_response()" name="" rows="" cols=""></textarea>';
    html += '  </div>';
    html += '</div>';

    html += '    </section>';
    html += '';
    html += '    <script type="text/x-template" id="terrain-list-template">';
    html += '      <div class="container">';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ terrainsMsg }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '        <div class="row farm-area mb-5">';
    //
    html += '          <section v-for="(terrain, index) in terrain_list" :key="index" data-terrain="terrain" class="col-sm-6 terrain" :class="terrain.name">';
    html += '            <div class="terrain-title text-center">';
    html += '              <p>{{ terrain.name }}</p>';
    html += '            </div>';
    html += '';
    html += '            <div class="terrain-body">';
    html += '              <p class="text-center">';
    html += '                <router-link :to="{name: \'terrain_details\', params: {id: index, name: terrain.name}}">Ver Terreno</router-link>';
    html += '              </p>';
    html += '            </div>';
    html += '          </section>';

    //
    html += '        </div>';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            <div class="text-center mb-5">';
    html += '              <a class="btn btn-outline-info" data-toggle="modal" data-target="#modal-new-terrain" href="#">Agregar terreno<\/a>';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';
    html += '      </div>';
    html += '      <!-- end container -->';
    html += '    </script>';
    html += '';
    html += '    <script type="tex/x-template" id="terrain-detail">';
    html += '      <div class="container">';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1> {{ terrainmsg }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '        <div class="row farm-area no-max-width mb-5">';
    html += '          <div class="col-sm-12 text-center" >';
    html += '            <h2 class="terrain-title">{{ name }}</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center loading" v-if="loading">';
    html += '            <h2>Loading...</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12" v-if="terrain">';
    html += '            <div class="row">';
    html += '              <div v-for="unidad in terrain" :key="unidad.id" class="col-sm-6 terrain-parcel terrain-unit text-center" v-bind:style="{ backgroundColor: unidad.unidadColor }">'
    html += '              <div class="row unit-row">';
    html += '                <div class="col-sm-12 text-center">';
    html += '                 <h2 class="unit-title">{{ unidad.unidadName }}</h2>';
    html += '                </div>';
    html += '                <div v-for="parcela in unidad.parcelas" :key="parcela.id" class="col-sm-6 text-center terrain-parcel" :class="parcela.parcelaName">';
    html += '                  <p>{{ parcela.parcelaName }}</p>';
    // html += '                  <p><router-link :to="{name: \'parcel_details\', params: { parcel_id: parcela.parcelaName }}">Ver detalle</router-link></p>';
    html += '                  <p><router-link :to="\'/parcel_details/\' + parcela.id">Ver detalle</router-link></p>';
    html += '                </div>'
    html += '                </div>'
    html += '              </div>';




    html += '            </div>';
    html += '          </div>';
    html += '';

    html += '        </div>'
    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            <div class="text-center mb-5">';
    html += '              <a class="btn btn-outline-info" data-toggle="modal" data-target="#modal-new-parcel" href="#">Agregar parcela</a>';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';

    //the modal for new parcela
    html += '    <!-- modal new parcel -->';
    html += '    <div class="modal fade" id="modal-new-parcel">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos de nueva parcela<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="new-parcel-form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Terreno:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="terrain-parcel" :value="name" :data-terrain-id="id">';
    // html += '                <\/select>';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Nombre de la Parcela:</label>';
    html += '                <input class="form-control" id="terrain-parcel-new-name" v-model="new_parcel_name">';
    html += '              </fieldset>';

    //ask for unit
    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Unidad de la parcela:</label>';
    html += '                <select class="form-control" id="" v-model="new_parcel_unit_selected">';
    html += '                  <option disabled value="">Seleccione la unidad de la parcela</option>';
    // html += '                  <option value="1">unidad prueba</option>';
    html += '                  <option v-for="unit in unidades_parcela" :key="unit.id" :value="unit.id">{{unit.unidadName}}</option>';
    html += '                </select>';
    html += '              </fieldset>';
    //end ask for unit

    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Tamaño de la parcela:<\/label>';
    html += '                <input type="number" min="1" name="" class="form-control" value="" v-model="new_parcel_size">';
    html += '                <select class="form-control" id="parcel-measure-unit" v-model="new_unit_selected">';
    html += '                  <option disabled value="">Seleccione la unidad de medida</option>';
    html += '                  <option v-for="unit in measure_units" :key="unit.id" :value="unit.id">{{unit.name}}</option>';
    html += '                <\/select>';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal" v-on:click="reset_parcel_modal()">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="dispatch_new_parcel()">Crear parcela<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end parcel modal -->';
    //end modal for new parcela

    html += '      </div>';
    html += '      <!-- end container -->';
    html += '    </script>';

    html += '<script type="text/x-template" id="parcel-detail-template">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area mb-5">';
    html += '          <div class="col-sm-12" v-if="loading">';
    html += '            <h2>Cargando datos de la parcela...</h2>';
    html += '          </div>';
    html += '          <div class="col-sm-12" v-if="parcel">';

    html += '            <div class="row" v-for="data in parcel" :key="data.id">';
    html += '              <div class="col-sm-12 text-center">'
    html += '               <h1>{{ data.name }}</h1>';
    html += '              </div>';
    html += '              <div v-for="tunel in data.tuneles" class="col-sm-6 text-center terrain-parcel" :class="tunel.name">';
    html += '                <p>{{ tunel.name }}</p>';
    // html += '                <p><router-link :to="{name: \'parcel_details\', params: {parcel_id: parcela.id}}">Ver detalle</router-link></p>';
    html += '              </div>'

    html += '            </div>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '      </div>';
    html += '</script>';

    html += '  <script type="text/x-template" id="tunel-details">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area mb-5">';
    html += '';
    html += '          <div class="col-sm-12" v-if="loading">';
    html += '            <h2>Cargando datos de tunel...</h2>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12" v-if="tunnel">';
    html += '            <div v-for="(tunel, index) in tunnel" :key="index" class="row">';
    html += '              <div v-for="(surco, i) in tunel" :key="i" class="col-sm-12 col-md-6 col-lg-4">';
    html += '                <div v-for="(planta, j) in surco" :key="j" class="row">';
    html += '                  <div class="col-sm-12">';
    html += '                    <p>{{ planta.name }}</p>';
    html += '                  </div>';
    html += '                </div>';
    html += '              </div>';
    html += '            </div>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12" v-if="error">';
    html += '';
    html += '            <h2>Error al cargar datos de Túnel... </h2>';
    html += '';
    html += '          </div>';
    html += '';
    html += '        </div>';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            ';
    html += '          </div>';
    html += '        </div>';
    html += '      </div>';
    html += '    </script>';


    //END HTML VUE-managed

    html += '<div class="container">';
    html += '  <div class="row">';
    html += '    <textarea class="col-sm-12" id="farm-data" name="" rows="" cols="">' + JSON.stringify(terrains) + '</textarea>';
    html += '    <textarea class="col-sm-12" id="measure-units" name="" rows="" cols="">' + JSON.stringify(measure_units) + '</textarea>';
    html += '    <textarea class="col-sm-12" id="response-result-units" name="" rows="" cols=""></textarea>';
    html += '  </div>';
    html += '</div>';

    html += '';
    html += '    <!-- modal -->';
    html += '    <div class="modal fade" id="modal-new-terrain">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos del nuevo terreno:<\/h4>';
    html += '';
    html += '';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form id="new-terrain-form" class="form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="parcel-name-field">Nombre: <\/label>';
    html += '                <input type="email" class="form-control" id="terrain-name-field" placeholder="Ingrese el nombre del terreno">';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" onclick="add_new_terrain()">Agregar Nuevo Terreno<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end modal -->';
    html += '';

    html += '';
    html += '    <!-- modal new tunnel -->';
    html += '    <div class="modal fade" id="modal-new-tunnel">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos para nuevo túnel<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form>';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="">Tamaño de túnel<\/label>';
    html += '                <input type="number" min="1" class="form-control" id="" placeholder="Ingrese el tamaño del túnel" value="1">';
    html += '              <\/fieldset>';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="">Seleccione unidad de medida<\/label>';
    html += '                <select class="form-control" id="">';
    html += '                  <option>1<\/option>';
    html += '                  <option>2<\/option>';
    html += '                  <option>3<\/option>';
    html += '                  <option>4<\/option>';
    html += '                  <option>5<\/option>';
    html += '                <\/select>';
    html += '              <\/fieldset>';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione la parcela<\/label>';
    html += '                <select class="form-control" id="">';
    html += '                  <option>1<\/option>';
    html += '                  <option>2<\/option>';
    html += '                  <option>3<\/option>';
    html += '                  <option>4<\/option>';
    html += '                  <option>5<\/option>';
    html += '                <\/select>';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info">Agregar nuevo túnel<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end modal new tunnel -->';
    html += '';

    html += '<div class="container">';
    html += '      <div class="row">';
    html += '        <div class="col-sm-12">';
    html += '          <div class="text-center mb-5 d-none">';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-terrain">Agregar Terreno<\/a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-terrain">Ver Terreno</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-units">Obtener unidades para parcelas</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-parcela">Agregar Parcela al Terreno</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-parcela">Ver Parcela</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-tunel">Agregar tunel a parcela</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-tunel">Ver detalle de tunel</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create-surco">Crear Surco</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detalle-surco">Ver Detalle Surco</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create-planta">Crear Planta</a>';
    html += '          <\/div>';
    html += '        <\/div>';
    html += '      <\/div>';
    html += '    <\/div>';


    html += '';
    html += '    <!-- build:js scripts\/vendor.js -->';
    html += '    <!-- bower:js -->';
    html += '    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>';
    html += '    <!-- endbower -->';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <!-- build:js scripts\/plugins.js -->';
    html += '';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <script src="https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/popper.js\/1.14.0\/umd\/popper.min.js" integrity="sha384-cs\/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY\/\/20VyM2taTB4QvJ" crossorigin="anonymous"><\/script>';
    html += '';
    html += '    <script src="https:\/\/stackpath.bootstrapcdn.com\/bootstrap\/4.1.0\/js\/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt\/6v9KI65qnm" crossorigin="anonymous"><\/script>';
    html += '';

    html += '    <!-- build:js scripts\/main.js -->';
    html += '    <!-- <script src="scripts\/main.js"><\/script> -->';
    html += '';
    html += '    <!--VUEJS dev version, includes helpful console warnings -->';
    html += '    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>';
    html += '';
    html += '    <!--VUE-ROUTER script -->';
    html += '    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>';

    html += '    <script src="https://system.netsuite.com/core/media/media.nl?id=3431&c=TSTDRV905028&h=626d2ae1b64ca26b8b97&_xt=.js"><\/script>';

    html += '    <!-- endbuild -->';
    html += '  <\/body>';
    html += '<\/html>';

    return html;
  }

  return {
    onRequest: main
  };
});
