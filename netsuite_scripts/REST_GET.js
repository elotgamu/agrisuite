/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */

define(['N/runtime', 'N/config', 'N/log'], function(runtime, config, log){
  function _get(context) {
    // var o = new Object();
    // o.sayhi = 'Hello World! ';
    // return o;
    return {
      'Message': 'Hello World!',
      'StatusCode': 2
    };
  }

  return {
    get: _get
  };
});
