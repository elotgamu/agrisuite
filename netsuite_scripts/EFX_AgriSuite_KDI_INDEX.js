/*******************************************************************
* @NApiVersion 2.x
* @NScriptType Suitelet
* Name: EFX_KDI_INDEX.js
*
* Author: Efficientix Dev Team
* Purpose: The base Suitelet for KDI
* Script: customscript_efx_kdi_agrisuite_index_sl
* Deploy: customdeploy_efx_kdi_agrisuite_index_sl
* ******************************************************************* */

define(['N/runtime', 'N/log', 'N/search', 'N/config', 'N/url', 'N/ui/serverWidget', 'N/https'], function (runtime, log, search, config, urlMod, ui, https) {

  function main(context) {

    var section = '';
    var scriptObj = runtime.getCurrentScript();
    var userObj = runtime.getCurrentUser();
    log.audit({ title: 'User', details: JSON.stringify(userObj) });

    var mode = context.request.parameters.custparam_mode || '';
    var script_id = context.request.parameters.custparam_script;
    var deployment_id = context.request.parameters.custparam_deploy;

    var this_script_id = 'customscript_efx_kdi_agrisuite_index_sl';
    var this_deploy_id = 'customdeploy_efx_kdi_agrisuite_index_sl';

    if (mode == 'resolveurl') {
      var responseResult = {
        statusCode: '',
        message: '',
        id: '',
        name: '',
        results: {}
      };
      if (script_id && deployment_id) {
        var url = resolve_url(script_id, deployment_id)

        responseResult.results = url;
        responseResult.statusCode = '2';
        responseResult.message = 'URL Obtenida'
      } else {
        responseResult.message = 'No se ha provisto id de script o deployment a resolver'
      }

      context.response.write(JSON.stringify(responseResult));
    } else {
      // get_display_data();

      section = 'Get Display Data';
      {
        try {

          section = 'Get Terrains';
          {
            var terrenoObj = {};

            // var scheme = 'https://';
            // var host = urlMod.resolveDomain({
            //   hostType: urlMod.HostType.APPLICATION
            // });

            var urlTerrain = urlMod.resolveScript({
              scriptId: 'customscript_efx_as_terreno_sl',
              deploymentId: 'customdeploy_efx_as_terreno_sl',
              returnExternalUrl: true
            });

            // urlTerrain = scheme + host + urlTerrain + '&custparam_mode=view';
            urlTerrain = urlTerrain + '&custparam_mode=view';

            log.audit({ title: 'urlTerrain', details: JSON.stringify(urlTerrain) });

            var responseTerrain = https.get({
              url: urlTerrain
            });

            log.audit({ title: 'responseTerrain', details: JSON.stringify(responseTerrain) });
            log.audit({ title: 'responseTerrain Code', details: JSON.stringify(responseTerrain.code) });
            log.audit({ title: 'responseTerrain Body', details: JSON.stringify(responseTerrain.body) });

            if (responseTerrain.code == 200) {
              if (responseTerrain.body) {
                var bodyObj = JSON.parse(responseTerrain.body);
                log.audit({ title: 'BodyObj', details: JSON.stringify(bodyObj) });
                log.audit({ title: 'responseTerrain Message', details: bodyObj.message });
                terrenoObj = bodyObj.results;
              }
            }

            log.audit({ title: 'Body', details: JSON.stringify(terrenoObj) });

          }

          //section get unit measures
          section = 'Get Unidad de Medidas';
          {
            var measure_units = {};
            var u_search = search.create({
              type: 'customrecord_efx_as_unidad_medida',
              filters: [
                ['isinactive', search.Operator.IS, 'F']
              ],
              columns: [
                { name: 'name' }
              ]
            });

            var results = u_search.run();
            var start = 0;

            do {
              var resultSet = results.getRange(start, start + 1000);
              if (resultSet && resultSet.length > 0) {
                for (var i = 0; i < resultSet.length; i++) {
                  var uId = resultSet[i].id;
                  var uName = resultSet[i].getValue({ name: 'name' }) || '';

                  if (!measure_units[uId]) {
                    measure_units[uId] = {
                      id: uId,
                      name: uName
                    };
                  }
                }
              }
              start += 1000;

            } while (resultSet && resultSet.length == 1000);
            log.audit({ title: 'Unidades de medidad', details: JSON.stringify(measure_units) });
          }
          //end get unidades de medidas

          section = 'GET THIS URL';
          {
            var this_url = resolve_url(this_script_id, this_deploy_id);
            var url_resolver = this_url + '&custparam_mode=resolveurl';
          }

          section = 'Create UI';
          {
            var form = ui.createForm({
              title: ' ',
              hideNavBar: false
            });


            var html = form.addField({
              id: 'custpage_html',
              type: ui.FieldType.INLINEHTML,
              label: 'HTML Content'
            });

            html.defaultValue = get_template(terrenoObj, measure_units, url_resolver);

            var fieldFrame = form.addField({
              id: 'custpage_selectedframe',
              label: 'Frame',
              type: ui.FieldType.INLINEHTML
            });

            fieldFrame.updateLayoutType({
              layoutType: ui.FieldLayoutType.OUTSIDEBELOW
            });

            fieldFrame.defaultValue = '<iframe id="terrain_frame" name="selected_frame" src="' + '  ' + '" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:scroll"></iframe>';

            context.response.writePage(form);

          }
          //end section create ui

        } catch (e) {
          throw e;
        }
      }
    }// end else



    // try {
    //
    //   section = 'Get Terrains';
    //   {
    //     var terrenoObj = {};
    //
    //     // var scheme = 'https://';
    //     // var host = urlMod.resolveDomain({
    //     //   hostType: urlMod.HostType.APPLICATION
    //     // });
    //
    //     var urlTerrain = urlMod.resolveScript({
    //       scriptId: 'customscript_efx_as_terreno_sl',
    //       deploymentId: 'customdeploy_efx_as_terreno_sl',
    //       returnExternalUrl: true
    //     });
    //
    //     // urlTerrain = scheme + host + urlTerrain + '&custparam_mode=view';
    //     urlTerrain = urlTerrain + '&custparam_mode=view';
    //
    //     log.audit({ title: 'urlTerrain', details: JSON.stringify(urlTerrain) });
    //
    //     var responseTerrain = https.get({
    //       url: urlTerrain
    //     });
    //
    //     log.audit({ title: 'responseTerrain', details: JSON.stringify(responseTerrain) });
    //     log.audit({ title: 'responseTerrain Code', details: JSON.stringify(responseTerrain.code) });
    //     log.audit({ title: 'responseTerrain Body', details: JSON.stringify(responseTerrain.body) });
    //
    //     if (responseTerrain.code == 200) {
    //       if (responseTerrain.body) {
    //         var bodyObj = JSON.parse(responseTerrain.body);
    //         log.audit({ title: 'BodyObj', details: JSON.stringify(bodyObj) });
    //         log.audit({ title: 'responseTerrain Message', details: bodyObj.message });
    //         terrenoObj = bodyObj.results;
    //       }
    //     }
    //
    //     log.audit({ title: 'Body', details: JSON.stringify(terrenoObj) });
    //
    //   }
    //
    //   //section get unit measures
    //   section = 'Get Unidad de Medidas';
    //   {
    //     var measure_units = {};
    //     var u_search = search.create({
    //       type: 'customrecord_efx_as_unidad_medida',
    //       filters: [
    //         ['isinactive', search.Operator.IS, 'F']
    //       ],
    //       columns: [
    //         { name: 'name' }
    //       ]
    //     });
    //
    //     var results = u_search.run();
    //     var start = 0;
    //
    //     do {
    //       var resultSet = results.getRange(start, start + 1000);
    //       if (resultSet && resultSet.length > 0) {
    //         for (var i = 0; i < resultSet.length; i++) {
    //           var uId = resultSet[i].id;
    //           var uName = resultSet[i].getValue({ name: 'name' }) || '';
    //
    //           if (!measure_units[uId]) {
    //             measure_units[uId] = {
    //               id: uId,
    //               name: uName
    //             };
    //           }
    //         }
    //       }
    //       start += 1000;
    //
    //     } while (resultSet && resultSet.length == 1000);
    //     log.audit({ title: 'Unidades de medidad', details: JSON.stringify(measure_units) });
    //   }
    //   //end get unidades de medidas
    //
    //   section = 'Create UI';
    //   {
    //     var form = ui.createForm({
    //       title: ' ',
    //       hideNavBar: false
    //     });
    //
    //
    //     var html = form.addField({
    //       id: 'custpage_html',
    //       type: ui.FieldType.INLINEHTML,
    //       label: 'HTML Content'
    //     });
    //
    //     html.defaultValue = get_template(terrenoObj, measure_units);
    //
    //     var fieldFrame = form.addField({
    //       id: 'custpage_selectedframe',
    //       label: 'Frame',
    //       type: ui.FieldType.INLINEHTML
    //     });
    //
    //     fieldFrame.updateLayoutType({
    //       layoutType: ui.FieldLayoutType.OUTSIDEBELOW
    //     });
    //
    //     fieldFrame.defaultValue = '<iframe id="terrain_frame" name="selected_frame" src="' + '  ' + '" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:scroll"></iframe>';
    //
    //     context.response.writePage(form);
    //
    //   }
    //   //end section create ui
    //
    // } catch (e) {
    //   throw e;
    // }
  }

  // function get_display_data() {
  //   try {
  //
  //     section = 'Get Terrains';
  //     {
  //       var terrenoObj = {};
  //
  //       // var scheme = 'https://';
  //       // var host = urlMod.resolveDomain({
  //       //   hostType: urlMod.HostType.APPLICATION
  //       // });
  //
  //       var urlTerrain = urlMod.resolveScript({
  //         scriptId: 'customscript_efx_as_terreno_sl',
  //         deploymentId: 'customdeploy_efx_as_terreno_sl',
  //         returnExternalUrl: true
  //       });
  //
  //       // urlTerrain = scheme + host + urlTerrain + '&custparam_mode=view';
  //       urlTerrain = urlTerrain + '&custparam_mode=view';
  //
  //       log.audit({ title: 'urlTerrain', details: JSON.stringify(urlTerrain) });
  //
  //       var responseTerrain = https.get({
  //         url: urlTerrain
  //       });
  //
  //       log.audit({ title: 'responseTerrain', details: JSON.stringify(responseTerrain) });
  //       log.audit({ title: 'responseTerrain Code', details: JSON.stringify(responseTerrain.code) });
  //       log.audit({ title: 'responseTerrain Body', details: JSON.stringify(responseTerrain.body) });
  //
  //       if (responseTerrain.code == 200) {
  //         if (responseTerrain.body) {
  //           var bodyObj = JSON.parse(responseTerrain.body);
  //           log.audit({ title: 'BodyObj', details: JSON.stringify(bodyObj) });
  //           log.audit({ title: 'responseTerrain Message', details: bodyObj.message });
  //           terrenoObj = bodyObj.results;
  //         }
  //       }
  //
  //       log.audit({ title: 'Body', details: JSON.stringify(terrenoObj) });
  //
  //     }
  //
  //     //section get unit measures
  //     section = 'Get Unidad de Medidas';
  //     {
  //       var measure_units = {};
  //       var u_search = search.create({
  //         type: 'customrecord_efx_as_unidad_medida',
  //         filters: [
  //           ['isinactive', search.Operator.IS, 'F']
  //         ],
  //         columns: [
  //           { name: 'name' }
  //         ]
  //       });
  //
  //       var results = u_search.run();
  //       var start = 0;
  //
  //       do {
  //         var resultSet = results.getRange(start, start + 1000);
  //         if (resultSet && resultSet.length > 0) {
  //           for (var i = 0; i < resultSet.length; i++) {
  //             var uId = resultSet[i].id;
  //             var uName = resultSet[i].getValue({ name: 'name' }) || '';
  //
  //             if (!measure_units[uId]) {
  //               measure_units[uId] = {
  //                 id: uId,
  //                 name: uName
  //               };
  //             }
  //           }
  //         }
  //         start += 1000;
  //
  //       } while (resultSet && resultSet.length == 1000);
  //       log.audit({ title: 'Unidades de medidad', details: JSON.stringify(measure_units) });
  //     }
  //     //end get unidades de medidas
  //
  //     section = 'Create UI';
  //     {
  //       var form = ui.createForm({
  //         title: ' ',
  //         hideNavBar: false
  //       });
  //
  //
  //       var html = form.addField({
  //         id: 'custpage_html',
  //         type: ui.FieldType.INLINEHTML,
  //         label: 'HTML Content'
  //       });
  //
  //       html.defaultValue = get_template(terrenoObj, measure_units);
  //
  //       var fieldFrame = form.addField({
  //         id: 'custpage_selectedframe',
  //         label: 'Frame',
  //         type: ui.FieldType.INLINEHTML
  //       });
  //
  //       fieldFrame.updateLayoutType({
  //         layoutType: ui.FieldLayoutType.OUTSIDEBELOW
  //       });
  //
  //       fieldFrame.defaultValue = '<iframe id="terrain_frame" name="selected_frame" src="' + '  ' + '" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:scroll"></iframe>';
  //
  //       context.response.writePage(form);
  //
  //     }
  //     //end section create ui
  //
  //   } catch (e) {
  //     throw e;
  //   }
  // }

  function get_template(terrains, measure_units, url_resolver) {
    var html = '';
    html += '<!doctype html>';
    html += '<html class="no-js" lang="es">';
    html += '  <head>';
    html += '    <meta charset="utf-8">';
    html += '    <meta name="description" content="KDI Agrisuite">';
    html += '    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
    html += '    <title>KDI-AgriSuite<\/title>';
    html += '';
    html += '    ';
    html += '    <!-- Place favicon.ico in the root directory -->';
    html += '';
    html += '    <!-- build:css styles\/vendor.css -->';
    html += '    <!-- bower:css -->';
    // html += '    <link rel="stylesheet" href="\/bower_components\/components-font-awesome\/css\/font-awesome.css" \/>';
    html += '    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">';
    html += '    <!-- endbower -->';
    html += '    <!-- endbuild -->';
    html += '    <link rel="stylesheet" href="https:\/\/stackpath.bootstrapcdn.com\/bootstrap\/4.1.0\/css\/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">';
    html += '';
    html += '';
    html += '    <!-- build:css styles\/main.css -->';
    html += '    <link rel="stylesheet" href="https://system.netsuite.com/core/media/media.nl?id=3430&c=TSTDRV905028&h=fc41008a6daa69ac89bd&_xt=.css">';
    // html += '    <link rel="stylesheet" href="https://system.na1.netsuite.com/c.3793201/suitebundle231071/main.css">';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <!-- build:js scripts\/vendor\/modernizr.js -->';
    // html += '    <script src="\/bower_components\/modernizr\/modernizr.js"><\/script>';
    html += '    <!-- endbuild -->';
    html += '  <\/head>';
    html += '  <body>';
    html += '    <!--[if IE]>';
    html += '      <p class="browserupgrade">You are using an <strong>outdated<\/strong> browser. Please <a href="http:\/\/browsehappy.com\/">upgrade your browser<\/a> to improve your experience.<\/p>';
    html += '    <![endif]-->';
    html += '';
    html += '    <div class="container">';
    html += '      <div class="header">';
    // html += '        <ul class="nav nav-pills float-right">';
    // html += '          <li class="nav-item">';
    // html += '            <a class="nav-link" href="#">Terrenos<\/a>';
    // html += '          <\/li>';
    // html += '          <li>';
    // html += '            <a class="nav-link" href="#">Parcelas<\/a>';
    // html += '          <\/li>';
    // html += '          <li>';
    // html += '            <a class="nav-link" href="#">Tuneles<\/a>';
    // html += '          <\/li>';
    // html += '        <\/ul>';
    html += '        <h3 class="text-muted font-weight-bold">AgriSuite<\/h3>';
    html += '      <\/div>';
    html += '';
    html += '    <\/div>';
    html += '';
    html += '';


    // THE HTML managed by the VUE instance
    html += '<section class="vue-area" id="farm-vue-area">';

    html += '   <div class="container">';
    html += '        <ul class="nav nav-pills">';
    html += '          <li class="nav-item">';
    html += '            <router-link class="nav-link" :to="{name: \'Terrenos\', params: {terrain_list: terrains }}">Terrenos</router-link>';
    html += '          </li>';
    html += '          <li class="nav-item">';
    // html += '            <a class="nav-link" href="#">Parcelas</a>';
    html += '          </li>';
    html += '          <li class="nav-item">';
    html += '            <router-link class="nav-link" :to="{name: \'Tuneles\', params: {tunnels: tunnels }}">Tuneles</router-link>';
    html += '          </li>';

    html += '          <li class="nav-item">';
    html += '            <router-link class="nav-link" :to="{name: \'Plantas\', params: {plants: plant_list }}">Plantas</router-link>';
    html += '          </li>';

    html += '        </ul>';
    html += '   </div>';

    // html += '<div class="container">';
    // html += ' <div class="row">';
    // html += ' <div class="col-sm-12 col-lg-6">';
    // html += ' <ol class="breadcrumb">';
    // html += '  <li v-for="(route, index) in $route.matched" class="breadcrumb-item">';
    // html += '    <router-link :to= "{name: route.name, params: {id: $route.params.id, name: $route.params.name }}">{{ route.name }}</router-link>';
    // html += '  </li>';
    // html += ' </ol>';
    // html += ' </div>';
    // html += ' </div>';
    // html += '</div>';

    //components will be rendered here
    html += '      <router-view :terrain_list="terrains"></router-view>';
    //component will be rendered here


    html += '<div class="container">';
    html += '  <div class="row">';
    html += '    <textarea class="col-sm-12" id="response_result" name="" rows="" cols=""></textarea>';
    html += '  </div>';
    html += '</div>';

    html += '    </section>';
    html += '';
    html += '    <script type="text/x-template" id="terrain-list-template">';
    html += '      <div class="container">';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ terrainsMsg }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '        <div class="row farm-area terrain-list mb-5">';
    //
    html += '          <section v-for="(terrain, index) in terrain_list" :key="index" data-terrain="terrain" class="col-sm-6 col-lg-4 terrain" :class="terrain.name">';
    html += '            <div class="terrain-title text-center">';
    html += '              <p class="font-weight-bold">{{ terrain.name }}</p>';
    html += '            </div>';
    html += '';
    html += '            <div class="terrain-body">';
    html += '              <p class="text-center">';
    html += '                <router-link :to="{name: \'Detalle de Terreno\', params: {id: index, name: terrain.name}}">Ver Terreno</router-link>';
    html += '              </p>';
    html += '            </div>';
    html += '          </section>';

    //
    html += '        </div>';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            <div class="text-center mb-5">';
    html += '              <a class="btn btn-outline-info" data-toggle="modal" data-target="#modal-new-terrain" href="#">Agregar terreno<\/a>';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';
    html += '      </div>';
    html += '      <!-- end container -->';
    html += '    </script>';
    html += '';
    html += '    <script type="tex/x-template" id="terrain-detail">';
    html += '      <div class="container">';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1> {{ terrainmsg }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '        <div class="row farm-area no-max-width mb-5">';
    html += '          <div class="col-sm-12 text-center" >';
    html += '            <h2 class="terrain-detail-title">{{ name }}</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center loading" v-if="loading">';
    html += '            <h2>Loading...</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12" v-if="terrain">';
    html += '            <div class="row">';
    html += '              <div v-for="unidad in terrain" :key="unidad.id" class="col-sm-6 terrain-parcel terrain-unit text-center" v-bind:style="{ backgroundColor: unidad.unidadColor }">'
    html += '              <div class="row unit-row">';
    html += '                <div class="col-sm-12 text-center">';
    html += '                 <h2 class="unit-title">{{ unidad.name }}</h2>';
    html += '                </div>';
    html += '                <div v-for="parcela in unidad.parcelas" :key="parcela.id" class="col-sm-6 text-center terrain-parcel" :class="parcela.parcelaName">';
    html += '                  <p class="font-weight-bold text-capitalize lead">{{ parcela.name }}</p>';
    html += '                  <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ parcela.tamanoparcela }} {{ parcela.unidad_medidaName }}</p>';
    html += '                  <p><router-link :to="{name: \'Detalle de Parcela\', params: { parcel_id: parcela.id, parcel_name: parcela.name }}">Ver detalle</router-link></p>';
    // html += '                  <p><router-link :to="\'/parcel/\' + parcela.id">Ver detalle</router-link></p>';
    html += '                </div>'
    html += '                </div>'
    html += '              </div>';


    html += '            </div>';
    html += '          </div>';
    html += '';

    html += '          <div class="col-sm-12 text-center" v-if="error">';
    html += '            <h2><i class="fa fa-info-circle" aria-hidden="true"></i> No hay parcelas en este terreno...</h2>';
    html += '          </div>';

    html += '        </div>'

    html += '        <div class="row">';
    html += '          <div class="col-sm-12">';
    html += '            <div class="text-center mb-5">';
    html += '              <a class="btn btn-outline-info" data-toggle="modal" data-target="#modal-new-parcel" href="#">Agregar parcela</a>';
    html += '            </div>';
    html += '          </div>';
    html += '        </div>';

    //the modal for new parcela
    html += '    <!-- modal new parcel -->';
    html += '    <div class="modal fade" id="modal-new-parcel">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos de nueva parcela<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="new-parcel-form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Terreno:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="terrain-parcel" :value="name" :data-terrain-id="id">';
    // html += '                <\/select>';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Nombre de la Parcela:</label>';
    html += '                <input class="form-control" id="terrain-parcel-new-name" v-model="new_parcel_name">';
    html += '              </fieldset>';

    //ask for unit
    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Unidad de la parcela:</label>';
    html += '                <select class="form-control" id="" v-model="new_parcel_unit_selected">';
    html += '                  <option disabled value="">Seleccione la unidad de la parcela</option>';
    // html += '                  <option value="1">unidad prueba</option>';
    html += '                  <option v-for="unit in unidades_parcela" :key="unit.id" :value="unit.id">{{unit.unidadName}}</option>';
    html += '                </select>';
    html += '              </fieldset>';
    //end ask for unit

    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Tamaño de la parcela:<\/label>';
    html += '                <input type="number" min="1" name="" class="form-control" value="" v-model="new_parcel_size">';
    html += '                <select class="form-control" id="parcel-measure-unit" v-model="new_unit_selected">';
    html += '                  <option disabled value="">Seleccione la unidad de medida</option>';
    html += '                  <option v-for="unit in measure_units" :key="unit.id" :value="unit.id">{{unit.name}}</option>';
    html += '                <\/select>';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal" v-on:click="reset_parcel_modal()">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="dispatch_new_parcel()">Crear parcela<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end parcel modal -->';
    //end modal for new parcela

    html += '      </div>';
    html += '      <!-- end container -->';
    html += '    </script>';

    html += '<script type="text/x-template" id="parcel-detail-template">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area mb-5">';
    html += '          <div class="col-sm-12" v-if="loading">';
    html += '            <h2>Cargando datos de la parcela...</h2>';
    html += '          </div>';
    html += '          <div class="col-sm-12" v-if="parcel">';

    html += '            <div class="row row-unit" >';
    html += '              <div class="col-sm-12 text-center">';
    html += '               <h2 class="parcel-detail-title text-capitalize">{{ parcel_name }}</h2>';
    html += '              </div>';
    html += '              <div v-for="data in parcel" :key="data.id" class="col-sm-6 terrain-parcel tunnel text-center">'
    html += '               <h1>{{ data.name }}</h1>';
    html += '               <p> ID: {{ data.id }}</p>';
    html += '                <p><router-link :to="{name: \'tunnel_details\', params: {tunnel_id: data.id, tunnel_name: data.name}}">Ver detalle</router-link></p>';
    html += '              </div>';
    // html += '              <div v-for="tunel in data.tuneles" class="col-sm-6 text-center terrain-parcel" :class="tunel.name">';
    // html += '                <p>{{ tunel.name }}</p>';
    // // html += '                <p><router-link :to="{name: \'parcel_details\', params: {parcel_id: parcela.id}}">Ver detalle</router-link></p>';
    // html += '              </div>';

    html += '            </div>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center" v-if="error">';
    html += '';
    html += '            <h2><i class="fa fa-info-circle" aria-hidden="true"></i> Este parcela no contiene túneles... </h2>';
    html += '';
    html += '          </div>';

    html += '        </div>';

    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '           <div class="mb-5">';
    html += '            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-new-tunel">Agregar túnel</a>';
    // html += '            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-add-plaga">Agregar plaga a parcela</a>';
    html += '           </div>';
    html += '          </div>';
    html += '        </div>';

    //modal goes here
    html += '    <!-- modal new tunel -->';
    html += '    <div class="modal fade" id="modal-new-tunel">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos del nuevo túnel<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Parcela:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="" :value="parcel_name">';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Nombre del túnel:</label>';
    html += '                <input class="form-control" id="" v-model="new_tunnel_name">';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Tamaño del tunel:</label>';
    html += '                <input type="number" min="1" name="" class="form-control" value="" v-model="new_tunnel_size">';
    html += '                <select class="form-control" id="parcel-measure-unit" v-model="new_measure_unit">';
    html += '                  <option disabled value="">Seleccione la unidad de medida</option>';
    html += '                  <option v-for="unit in measure_units" :key="unit.id" :value="unit.id">{{unit.name}}</option>';
    html += '                </select>';
    html += '              </fieldset>';
    html += '';
    html += '            </form>';
    html += '          </div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="send_create_tunnel()">Crear túnel</button>';
    html += '          </div>';
    html += '        </div><!-- /.modal-content -->';
    html += '      </div><!-- /.modal-dialog -->';
    html += '    </div><!-- /.modal -->';
    html += '    <!-- end parcel modal -->';
    //modal ends here

    html += '';
    html += '      </div>';
    html += '</script>';

    html += '  <script type="text/x-template" id="tunel-details">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area farm-area-tunnel mb-5">';
    html += '';

    html += '          <div class="col-sm-12 text-center">';
    html += '            <h2 class="font-weight-bold">{{ tunnel_name }}</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center" v-if="loading">';
    html += '            <h2>Cargando datos de tunel...</h2>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12 text-center" v-if="tunnel">';
    html += '            <div class="row">';
    html += '              <div v-for="surco in tunnel" class="col-sm-12 col-md-6 col-lg-4 terrain-parcel groove">';
    // html += '                <div v-for="(planta, j) in surco" :key="j" class="row">';
    // html += '                  <div class="col-sm-12">';
    html += '                    <p class="font-weight-bold lead text-capitalize">{{ surco.name }}</p>';
    html += '                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ surco.tamano }} {{ surco.name_unidad_med }}</p>';
    html += '                    <p> <router-link :to="{ name: \'groove_details\', params: {groove_id: surco.id, groove_name: surco.name}}">Ver Detalles</router-link> </p>';
    // html += '                  </div>';
    // html += '                </div>';
    html += '              </div>';
    html += '            </div>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12 text-center" v-if="error">';
    html += '';
    html += '            <h2><i class="fa fa-info-circle" aria-hidden="true"></i> Este túnel no contiene surcos... </h2>';
    html += '';
    html += '          </div>';
    html += '';
    html += '        </div>';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '           <div class="mb-5">';
    html += '            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-new-surco">Agregar Surco</a>';
    html += '           </div>';
    html += '          </div>';
    html += '        </div>';


    //modal new surco goes here
    html += '    <!-- modal new tunel -->';
    html += '    <div class="modal fade" id="modal-new-surco">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos para el nuevo surco<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Túnel:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="" :value="tunnel_name">';
    // html += '                <\/select>';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Nombre del surco:</label>';
    html += '                <input class="form-control" id="" v-model="new_surco.name">';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Tamaño del surco:</label>';
    html += '                <input type="number" min="1" name="" class="form-control" value="" v-model="new_surco.size">';
    html += '                <select class="form-control" id="parcel-measure-unit" v-model="new_surco.measure_unit">';
    html += '                  <option disabled value="">Seleccione la unidad de medida</option>';
    html += '                  <option v-for="unit in measure_units" :key="unit.id" :value="unit.id">{{ unit.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';
    html += '';
    html += '            </form>';
    html += '          </div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="send_create_groove()">Crear Surco</button>';
    html += '          </div>';
    html += '        </div><!-- /.modal-content -->';
    html += '      </div><!-- /.modal-dialog -->';
    html += '    </div><!-- /.modal -->';
    html += '    <!-- end parcel modal -->';
    //modal new surco ends here

    html += '      </div>';
    html += '    </script>';

    html += '  <script type="text/x-template" id="surco-details">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area mb-5">';
    html += '';

    html += '          <div class="col-sm-12 text-center">';
    html += '            <h2>{{ groove_name }}</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center" v-if="loading">';
    html += '            <h2>Cargando datos de este surco...</h2>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12 text-center" v-if="groove">';
    html += '            <div class="row">';
    html += '              <div v-for="crop in groove" class="col-sm-12 col-md-6 col-lg-4 terrain-parcel crop">';
    // // html += '                <div v-for="(planta, j) in surco" :key="j" class="row">';
    // // html += '                  <div class="col-sm-12">';
    html += '                    <p>{{ crop.name }}</p>';
    html += '                    <p> <router-link :to="{name: \'Detalle de Cultivo\', params: {id: crop.id, name: crop.name}}">Ver Detalles</router-link> </p>';
    // // html += '                  </div>';
    // // html += '                </div>';
    html += '              </div>';
    html += '            </div>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12 text-center" v-if="error">';
    html += '';
    html += '            <h2> <i class="fa fa-info-circle" aria-hidden="true"></i> Este surco aún no contiene cultivos... </h2>';
    html += '';
    html += '          </div>';
    html += '';
    html += '        </div>';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '           <div class="mb-5">';
    html += '            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-new-cultivo">Agregar Cultivo</a>';
    // html += '            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="">Agregar Planta</a>';
    html += '           </div>';
    html += '          </div>';
    html += '        </div>';


    //modal new surco goes here
    html += '    <!-- modal new cultivo -->';
    html += '    <div class="modal fade" id="modal-new-cultivo">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos para el nuevo cultivo<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Cultivo:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="" :value="groove_name">';
    // html += '                <\/select>';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Nombre del cultivo:</label>';
    html += '                <input class="form-control" id="" v-model="new_crop.name">';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Densidad del cultivo:</label>';
    html += '                <input type="number" class="form-control" id="" v-model="new_crop.density">';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione planta:</label>';
    html += '                <select class="form-control" id="" v-model="new_crop.plant">';
    html += '                  <option disabled value="">Seleccione la planta</option>';
    html += '                  <option v-for="plant in plants" :key="plant.id" :value="plant.id">{{ plant.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione tipo de cultivo:</label>';
    html += '                <select class="form-control" id="" v-model="new_crop.crop_type">';
    html += '                  <option disabled value="">Seleccione el tipo de cultivo</option>';
    html += '                  <option v-for="type in crop_types" :key="type.id" :value="type.id">{{ type.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione tipo de variedad:</label>';
    html += '                <select class="form-control" id="" v-model="new_crop.crop_type_variety">';
    html += '                  <option disabled value="">Seleccione el tipo de variedad</option>';
    html += '                  <option v-for="variety in crop_variety_types" :key="variety.id" :value="variety.id">{{ variety.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group inline-fieldset">';
    html += '                <label for="exampleSelect1">Tamaño del cultivo:</label>';
    html += '                <input type="number" min="1" name="" class="form-control" value="" v-model="new_crop.size">';
    html += '                <select class="form-control" id="parcel-measure-unit" v-model="new_crop.measure_unit">';
    html += '                  <option disabled value="">Seleccione la unidad de medida</option>';
    html += '                  <option v-for="unit in measure_units" :key="unit.id" :value="unit.id">{{ unit.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';
    html += '';
    html += '            </form>';
    html += '          </div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="send_create_crop()">Crear Cultivo</button>';
    html += '          </div>';
    html += '        </div><!-- /.modal-content -->';
    html += '      </div><!-- /.modal-dialog -->';
    html += '    </div><!-- /.modal -->';
    html += '    <!-- end cultivo modal -->';
    //modal new cultivo ends here

    html += '      </div>';
    html += '    </script>';

    //template for detalle_cultivo
    html += '  <script type="text/x-template" id="cultivo-details">';
    html += '      <div class="container">';
    html += '';
    html += '        <div class="row">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div>';
    html += '        </div>';
    html += '';
    html += '        <div class="row farm-area mb-5">';
    html += '';

    html += '          <div class="col-sm-12 text-center">';
    html += '            <h2>{{ name }}</h2>';
    html += '          </div>';

    html += '          <div class="col-sm-12 text-center" v-if="loading">';
    html += '            <h2>Cargando datos de este cultivo...</h2>';
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12" v-if="crop">';
    html += '            <p class="text-center" v-if="crop.plaga">';
    html += '             <span v-if="!crop.fecha_fin"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Este cultivo tiene una plaga no controlada..</span>';
    html += '             <span v-if="crop.fecha_fin"><i class="fa fa-info-circle" aria-hidden="true"></i> Este cultivo tuvo una plaga de {{ crop.plaga }} controlada el {{ crop.fecha_fin }}</span>';
    html += '            </p>';
    html += '            <div class="row">';
    // html += '              <div v-for="plant in crop" class="col-sm-12 terrain-parcel">';
    // // // html += '                <div v-for="(planta, j) in surco" :key="j" class="row">';
    html += '                  <div class="col-sm-12 terrain-parcel plant" v-if="crop.planta">';

    html += '                    <p class="text-center"><span class="font-weight-bold"><i class="fa fa-leaf" aria-hidden="true"></i> Planta:</span> {{ crop.planta }}</p>';
    html += '                    <p class="text-center"><span class="font-weight-bold"><i class="fa fa-pagelines" aria-hidden="true"></i> Tipo Variedad:</span> {{ crop.tipo_variedad }}</p>';
    // //html += '                    <p> <router-link :to{name: \'\', params: {}}>Ver Detalles</router-link> </p>';
    html += '                  </div>';
    // // // html += '                </div>';
    // html += '              </div>';

    html += '            </div>';


    //modal goes here due to crop_id
    //modal add plaga
    html += '    <!-- modal add plaga to parcela -->';
    html += '    <div class="modal fade" id="modal-add-plaga">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos de la plaga<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form class="form" id="">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Cultivo:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="" :value="name">';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione la plaga:</label>';
    html += '                <select class="form-control" v-model="new_plague.id">';
    html += '                  <option disabled value="">Seleccione la plaga</option>';
    html += '                  <option v-for="plague in plagues" :key="plague.id" :value="plague.id">{{ plague.name }}</option>';
    html += '                </select>';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Fecha reportada:</label>';
    html += '                <input type="date" class="form-control" v-model="new_plague.date_reported">';
    html += '              </fieldset>';
    html += '';
    html += '            </form>';
    html += '          </div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="add_plague_to_crop()">Reportar Plaga</button>';
    html += '          </div>';
    html += '        </div><!-- /.modal-content -->';
    html += '      </div><!-- /.modal-dialog -->';
    html += '    </div><!-- /.modal -->';
    html += '    <!-- end add plaga -->';
    //end modal add plaga

    //modal remove plaga
    html += '    <!-- modal add plaga to parcela -->';
    html += '    <div class="modal fade" id="modal-delete-plaga">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos de la plaga<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    //render the form until crop has been fteched
    html += '            <form class="form" id="" v-if="crop">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Cultivo:</label>';
    html += '                <input readonly class="form-control-plaintext font-weight-bold" id="" :value="name">';
    html += '              </fieldset>';

    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Seleccione la plaga:</label>';
    html += '                <select class="form-control" v-model="plague_killed.id">';
    html += '                  <option disabled value="">Seleccione la plaga</option>';
    html += '                  <option :value="crop.id_plaga" selected>{{ crop.plaga }}</option>';
    html += '                </select>';
    html += '              </fieldset>';

    html += '              <fieldset class="form-group">';
    html += '                <label for="exampleSelect1">Fecha controlada:</label>';
    html += '                <input type="date" class="form-control" v-model="plague_killed.date_finished">';
    html += '              </fieldset>';
    html += '';
    html += '            </form>';
    html += '          </div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="control_plague()">Guardar</button>';
    html += '          </div>';
    html += '        </div><!-- /.modal-content -->';
    html += '      </div><!-- /.modal-dialog -->';
    html += '    </div><!-- /.modal -->';
    html += '    <!-- end add plaga -->';
    //end modal remove plaga

    //modal activo productivo
    html += '<div class="modal fade" id="modal-activo-productivo">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Activo Productivo<\/h4>';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <div class="form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="">Nombre:</label>';
    html += '                <input type="text" class="form-control" placeholder="Nombre" v-model="new_activo.name">';
    html += '';
    html += '              <\/fieldset>';
    html += '              <fieldset class="form-group">';
    html += '                <label >Fecha inicio temporada: </label>';
    html += '                <div class="input-group">';
    html += '                  <div class="input-group-prepend">';
    html += '                    <span class="input-group-text" id=""> <i class="fa fa-calendar"><\/i> <\/span>';
    html += '                  <\/div>';
    html += '                  <input type="date" class="form-control" aria-describedby="" v-model="new_activo.date">';
    html += '                <\/div>';
    html += '                ';
    html += '              <\/fieldset>';
    html += '              <fieldset class="form-group">';
    html += '                <label >Meses:<\/label>';
    html += '                <input type="number" class="form-control" placeholder="Meses" v-model="new_activo.months">';
    html += '              <\/fieldset>';
    html += '              <fieldset class="form-group">';
    html += '                <label for="">Curva:<\/label>';
    html += '                <input type="text" readonly class="form-control-plaintext" name="" value="100%" v-model="new_activo.curve">';
    html += '';
    html += '              <\/fieldset>';
    html += '              <fieldset class="form-group">';
    html += '                <label for="">Presupuesto de gasto<\/label>';
    html += '                <div class="input-group">';
    html += '                  <div class="input-group-prepend">';
    html += '                    <span class="input-group-text" id="">$<\/span>';
    html += '                  <\/div>';
    html += '                  <input type="text" class="form-control" placeholder="0.00" aria-describedby="" v-model="new_activo.sp_budget">';
    html += '                <\/div>';
    html += '              <\/fieldset>';
    html += '';
    // html += '              <fieldset class="form-group">';
    // html += '                <label for="">Cajas:<\/label>';
    // html += '                <input type="number" class="form-control" name="" value="" v-model="new_activo.boxes">';
    // html += '              <\/fieldset>';
    // html += '              <fieldset>';
    // html += '                <label for="">Estimado de costo:<\/label>';
    // html += '                <div class="input-group">';
    // html += '                  <div class="input-group-prepend">';
    // html += '                    <span class="input-group-text" id="inputGroupPrepend3">$<\/span>';
    // html += '                  <\/div>';
    // html += '                  <input type="text" class="form-control" placeholder="0.00" aria-describedby="" v-model="new_activo.est_cost">';
    // html += '                <\/div>';
    // html += '              <\/fieldset>';
    html += '            <\/div>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="call_create_actv()">Registar activo productivo<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';

    //end modal activo productivo
    //end modal goes here
    html += '          </div>';
    html += '';
    html += '          <div class="col-sm-12 text-center" v-if="empty">';
    html += '';
    html += '            <h2> <i class="fa fa-info-circle" aria-hidden="true"></i> Este cultivo vacio </h2>';
    html += '';
    html += '          </div>';
    html += '';
    html += '        </div>';
    html += '';
    html += '        <div class="row" v-if="crop">';
    html += '          <div class="col-sm-12 text-center">';
    html += '           <div class="mb-5">';

    html += '            <input type="button" class="btn btn-outline-danger" :disabled="can_report_plague" href="#" data-toggle="modal" data-target="#modal-add-plaga" value="Reportar Plaga">';
    html += '            <input type="button" class="btn btn-outline-info" :disabled="can_control_plague" href="#" data-toggle="modal" data-target="#modal-delete-plaga" value="Controlar Plaga">';

    html += '            <input type="button" class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-activo-productivo" value="Registar activo productivo">';

    html += '           </div>';
    html += '          </div>';
    html += '        </div>';




    html += '      </div>';
    html += '    </script>';
    //template for detalle cultivo

    //template for tunnel list components
    html += '<script type="text/text-xtemplate" id="tunnel-list-view">';
    html += '  <div class="container">';
    html += '    <div class="row">';
    html += '      <div class="col-sm-12 text-center">';
    html += '        <h1>{{ message }}</h1>';
    html += '      </div>';
    html += '    </div>';

    html += '    <div class="row farm-area mb-5">';
    html += '      <div class="col-sm-12" v-if="loading">';
    html += '        <h2>Cargando los túneles...</h2>';
    html += '      </div>';

    html += '    <div class="col-sm-12" v-if="tunel_list">';
    html += '      <div class="row">';
    html += '        <div v-for="(tunnel, index) in tunel_list" :key="index" class="col-sm-6 terrain-parcel">';
    html += '          <p>{{ tunnel.name }}</p>';
    html += '        </div>';
    html += '      </div>';
    html += '    </div>';

    html += '    <div class="col-sm-12" v-if="empty">';
    html += '      <h2>No se pudo cargar los datos de tuneles</h2>';
    html += '    </div>';
    html += '  </div>';
    html += '</div>';
    html += '</script>';
    //end tempate for tunnel list component

    //template for plants list components
    html += '<script type="text/text-xtemplate" id="plant-list-view">';
    html += '  <div class="container">';
    html += '    <div class="row">';
    html += '      <div class="col-sm-12 text-center">';
    html += '        <h1>{{ message }}</h1>';
    html += '      </div>';
    html += '    </div>';

    html += '    <div class="row farm-area mb-5">';
    html += '      <div class="col-sm-12" v-if="loading">';
    html += '        <h2>Cargando las plantas...</h2>';
    html += '      </div>';

    html += '    <div class="col-sm-12" v-if="plants_list">';
    html += '      <div class="row">';
    html += '        <div v-for="(planta, index) in plants_list" :key="index" class="col-sm-6 terrain-parcel">';
    html += '          <p>{{ planta.name }}</p>';
    html += '        </div>';
    html += '      </div>';
    html += '    </div>';

    html += '    <div class="col-sm-12" v-if="empty">';
    html += '      <h2>No se han registrado plantas...</h2>';
    html += '    </div>';
    html += '  </div>';

    html += '  <div class="row">';
    html += '    <div class="col-sm-12 text-center">';
    html += '       <div class="mb-5">';
    html += '         <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#modal-new-plant">Agregar Planta</a>';
    html += '       </div>';
    html += '   </div>';

    html += '</div>';

    html += '    <!-- modal -->';
    html += '    <div class="modal fade" id="modal-new-plant">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos de la planta a ingresar:<\/h4>';
    html += '';
    html += '';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form id="new-terrain-form" class="form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="parcel-name-field">Nombre de la planta: <\/label>';
    html += '                <input type="text" class="form-control" id="" placeholder="Ingrese de la planta" value="" v-model="new_plant.name">';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" v-on:click="create_new_plant()">Agregar Nueva Planta<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end modal -->';
    html += '</div>';
    html += '</script>';
    //end tempate for plants list component

    //template for active asset creation -->
    html += '<script type="text/x-template" id="asset-creation">';
    html += '      <div class="container">';
    html += '        <div class="row mb-2">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h1>{{ message }}</h1>';
    html += '          </div><!-- end col- meesage -->';
    html += '        </div><!-- end row message -->';
    html += '';
    html += '        <div class="row mb-2" v-if="loading">';
    html += '          <div class="col-sm-12 text-center">';
    html += '            <h3 class="font-weight-bold">Cargando activo productivo...</h3>';
    html += '            <i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i>';
    html += '          </div>';
    html += '        </div><!-- end row v-if loading -->';
    html += '';
    html += '        <div class="row mb-2" v-if="prod_asset">';

    html += '          <div class="col-sm-12 mb-2 text-center">';
    html += '            <input type="button" class="btn btn-info" value="Crear Activo Productivo" v-on:click="send_create_actv()">';
    html += '            <input type="button" class="btn btn-outline-danger" value="Cancelar">';
    html += '          </div>';

    html += '          <div class="col-sm-12">';
    html += '            <div class="table-responsive">';
    html += '              <table class="table table-striped table-hover" v-if="prod_asset.actv_detail" id="actv-create-table">';
    html += '                <thead>';
    html += '                  <tr>';
    html += '                    <th>Mes</th>';
    html += '                    <th>Curva</th>';
    html += '                    <th>Presupuesto Gasto</th>';
    html += '                    <th>Cajas</th>';
    html += '                    <th>Costo Estimado de producción</th>';
    html += '                  </tr>';
    html += '                </thead>';
    html += '                <tbody>';
    html += '                  <tr v-for="month in prod_asset.actv_detail">';

    html += '                    <th scope="row"> {{ month.month }} </th>';

    html += '                    <td>';
    html += '                      <div class="input-group">';
    html += '                        <div class="input-group-prepend">';
    html += '                          <span class="input-group-text" id="">$</span>';
    html += '                        </div>';
    html += '                        <input type="number" v-model="month.curve_percent" class="form-control">';
    html += '                      </div>';
    html += '                    </td>';


    html += '                    <td>';
    html += '                      <div class="input-group">';
    html += '                        <div class="input-group-prepend">';
    html += '                          <span class="input-group-text" id="">$</span>';
    html += '                        </div>';
    html += '                        <input type="number" v-model="month.budget_sp" class="form-control">';
    html += '                      </div>';
    html += '                    </td>';

    html += '                    <td>';
    html += '                      <div class="input-group">';
    html += '                        <div class="input-group-prepend">';
    html += '                          <span class="input-group-text" id="">$</span>';
    html += '                        </div>';
    html += '                        <input type="number" v-model="month.est_box" class="form-control">';
    html += '                      </div>';
    html += '                    </td>';

    html += '                    <td>';
    html += '                      <div class="input-group">';
    html += '                        <div class="input-group-prepend">';
    html += '                          <span class="input-group-text" id="">$</span>';
    html += '                        </div>';
    html += '                        <input type="number" v-model="month.est_cost" class="form-control">';
    html += '                      </div>';
    html += '                    </td>';

    html += '                  </tr>';
    html += '                </tbody>';
    html += '              </table>';
    html += '            </div><!-- end table responsive -->';
    html += '          </div><!-- end col-sm 12 table -->';
    html += '';
    html += '          <div class="col-sm-12">';
    html += '';
    html += '          </div>';
    html += '        </div><!-- end row v-if prod-asset -->';
    html += '';
    html += '      </div>';
    html += '    </script>';
    //end template for active asset creation -->

    //END HTML VUE-managed

    html += '<div class="container">';
    html += '  <div class="row">';
    html += '    <textarea class="col-sm-12" id="farm-data" name="" rows="" cols="">' + JSON.stringify(terrains) + '</textarea>';
    html += '    <textarea class="col-sm-12" id="measure-units" name="" rows="" cols="">' + JSON.stringify(measure_units) + '</textarea>';
    html += '    <textarea class="col-sm-12" id="response-result-units" name="" rows="" cols=""></textarea>';
    html += '    <textarea class="col-sm-12" id="response-result-units" name="" rows="" cols="">'+ url_resolver +'</textarea>';
    html += '  </div>';
    html += '</div>';

    html += '';
    html += '    <!-- modal -->';
    html += '    <div class="modal fade" id="modal-new-terrain">';
    html += '      <div class="modal-dialog" role="document">';
    html += '        <div class="modal-content">';
    html += '          <div class="modal-header">';
    html += '            <h4 class="modal-title">Datos del nuevo terreno:<\/h4>';
    html += '';
    html += '';
    html += '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += '              <span aria-hidden="true">&times;<\/span>';
    html += '              <span class="sr-only">Close<\/span>';
    html += '            <\/button>';
    html += '';
    html += '          <\/div>';
    html += '          <div class="modal-body">';
    html += '            <form id="new-terrain-form" class="form">';
    html += '';
    html += '              <fieldset class="form-group">';
    html += '                <label for="parcel-name-field">Nombre: <\/label>';
    html += '                <input type="email" class="form-control" id="terrain-name-field" placeholder="Ingrese el nombre del terreno">';
    html += '              <\/fieldset>';
    html += '';
    html += '            <\/form>';
    html += '          <\/div>';
    html += '          <div class="modal-footer">';
    html += '            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar<\/button>';
    html += '            <button type="button" class="btn btn-outline-info" onclick="add_new_terrain()">Agregar Nuevo Terreno<\/button>';
    html += '          <\/div>';
    html += '        <\/div><!-- \/.modal-content -->';
    html += '      <\/div><!-- \/.modal-dialog -->';
    html += '    <\/div><!-- \/.modal -->';
    html += '    <!-- end modal -->';
    html += '';
    html += '';

    html += '<div class="container">';
    html += '      <div class="row">';
    html += '        <div class="col-sm-12">';
    html += '          <div class="text-center mb-5 d-none">';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-terrain">Agregar Terreno<\/a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-terrain">Ver Terreno</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-units">Obtener unidades para parcelas</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-parcela">Agregar Parcela al Terreno</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-parcela">Ver Parcela</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-add-tunel">Agregar tunel a parcela</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detail-tunel">Ver detalle de tunel</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-list-tunel">Obtener todos los tunel</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create-surco">Crear Surco</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detalle-surco">Ver Detalle Surco</a>';

    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-plants">Obtener Plantas</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create-planta">Crear Planta</a>';
    //
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-crop-types">Obtener Tipo de cultivos</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-crop-variety-types">Obtener Tipos de Variedad</a>';

    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create-crop">Crear cultivo</a>';
    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-detalle-crop">Ver detalle Cultivo</a>';

    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-get-plages">Obtener plagas</a>';

    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-plague-to-crop">Agregar plaga a cultivo</a>';


    html += '            <a class="btn btn-outline-info" href="#" onclick="" id="btn-create_actv-prod">Crear Activo Productivo</a>';

    html += '          <\/div>';
    html += '        <\/div>';
    html += '      <\/div>';
    html += '    <\/div>';


    html += '';
    html += '    <!-- build:js scripts\/vendor.js -->';
    html += '    <!-- bower:js -->';
    html += '    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>';
    html += '    <!-- endbower -->';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <!-- build:js scripts\/plugins.js -->';
    html += '';
    html += '    <!-- endbuild -->';
    html += '';
    html += '    <script src="https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/popper.js\/1.14.0\/umd\/popper.min.js" integrity="sha384-cs\/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY\/\/20VyM2taTB4QvJ" crossorigin="anonymous"><\/script>';
    html += '';
    html += '    <script src="https:\/\/stackpath.bootstrapcdn.com\/bootstrap\/4.1.0\/js\/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt\/6v9KI65qnm" crossorigin="anonymous"><\/script>';
    html += '';

    html += '    <!-- build:js scripts\/main.js -->';
    html += '    <!-- <script src="scripts\/main.js"><\/script> -->';

    html += '    <!--- AXIOS -->';
    html += '    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>';

    html += '    <!-- MomentJS -->';
    html += '    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>';
    html += '';
    html += '    <!--VUEJS dev version, includes helpful console warnings -->';
    // html += '    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>';
    html += '    <script src="https://unpkg.com/vue/dist/vue.js"></script>';
    html += '';
    html += '    <!--VUE-ROUTER script -->';
    html += '    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>';

    html += '    <script src="https://system.netsuite.com/core/media/media.nl?id=3431&c=TSTDRV905028&h=626d2ae1b64ca26b8b97&_xt=.js"><\/script>';
    // html += '    <script src="https://system.na1.netsuite.com/c.3793201/suitebundle231071/agrisuite-main-script.js"><\/script>';

    html += '    <!-- endbuild -->';
    html += '  <\/body>';
    html += '<\/html>';

    return html;
  }

  function resolve_url(script, deploy) {
    var resolved_url = urlMod.resolveScript({
      scriptId: script,
      deploymentId: deploy,
      returnExternalUrl: false
    });
    log.audit({title: 'URL resuelta', details: resolved_url})
    return resolved_url;
  }

  return {
    onRequest: main
  };
});
