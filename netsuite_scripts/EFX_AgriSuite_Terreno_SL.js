/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

define(['N/record', 'N/search', 'N/ui/serverWidget'], function (record, search, ui) {
  function onRequest(context) {
    var section = '';

    try {

      if (context.request.method == 'GET') {

        section = 'Get Parameters';
        {
          //view
          //create
          //update
          //delete
          var mode = context.request.parameters.custparam_mode || '';
          var id = context.request.parameters.custparam_id || '';
          var terreno = context.request.parameters.custparam_terreno || '';
          log.audit({ title: 'Parametros:', details: 'Mode: ' + mode + ' | Id: ' + id + ' | Terreno: ' + terreno });

          var responseResult = {
            statusCode: '',
            message: '',
            id: '',
            name: '',
            results: {}
          };
        }

        if (mode == 'view') {

          var terrenoObj = {};
          section = 'Get Terrenos';
          {
            var result = search.create({
              type: 'customrecord_efx_as_terreno',
              filters: [
                ['isinactive', search.Operator.IS, 'F']
              ],
              columns: [
                { name: 'name' }
              ]
            });

            var resultData = result.run();
            var start = 0;

            do {
              var resultSet = resultData.getRange(start, start + 1000);
              if (resultSet && resultSet.length > 0) {
                for (var i = 0; i < resultSet.length; i++) {
                  var rId = resultSet[i].id;
                  var rName = resultSet[i].getValue({ name: 'name' }) || '';

                  if (!terrenoObj[rId]) {
                    terrenoObj[rId] = {
                      name: rName
                    };
                  }
                }
              }
              start += 1000;

            } while (resultSet && resultSet.length == 1000)

            responseResult.statusCode = 2;
            responseResult.message = 'Lista Obtenida Exitosamente!';
            responseResult.results = terrenoObj;
          }
        }
        else if (mode == 'create') {
          if (terreno) {
            section = 'Create Terreno';
            {
              var rec = record.create({
                type: 'customrecord_efx_as_terreno'
              });

              rec.setValue({
                fieldId: 'name',
                value: terreno
              });

              var recId = rec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
              });

              responseResult.statusCode = 2;
              responseResult.message = 'Creado Exitosamente!';
              responseResult.id = recId;
              responseResult.name = terreno;

              var terrain_list = get_all_terrains();
            }
          }
          else {
            responseResult.statusCode = 3;
            responseResult.message = 'Error, Es necesario el parametro Terreno!';
          }
        }
        else if (mode == 'detailview') {
          if (id) {
            var terreno_obtenido = get_terrain_details(id);
            if (terreno_obtenido == []) {
              responseResult.statusCode = 3;
              responseResult.message = 'Error, NO se pudo obtener el terreno';
            } else {
              responseResult.statusCode = 2;
              responseResult.message = 'Se ha obtenido el terreno solicitado';
              responseResult.results = terreno_obtenido;
            }

          } else {
            responseResult.statusCode = 3;
            responseResult.message = 'Error, Es necesario el parametro id del  Terreno!';
          }
        }

        // section = 'Create Form';
        // {
        //   var form = ui.createForm({
        //     title: ' ',
        //     hideNavBar: true
        //   });

        //   var html = form.addField({
        //     id: 'custpage_html',
        //     type: ui.FieldType.INLINEHTML,
        //     label: 'HTML Content'
        //   });

        //   if (mode == 'create') {
        //     html.defaultValue = JSON.stringify(responseResult) + '<div id="terrain_list" name="selected_frame" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" vspace="0" hspace="0" style="overflow:scroll">'+ JSON.stringify(terrain_list) + '</div> <script type="text/javascript">parent.App.terrains = JSON.parse(document.getElementById(\'terrain_list\').innerHTML);</script>';
        //   } else if (mode == 'detailview') {
        //     html.defaultValue = JSON.stringify(responseResult) + '<div id="terrain_details_result" name="selected_frame" style="padding:5px;" scrolling="auto" width="100%" height="700" marginwidth="0" marginheight="0" vspace="0" hspace="0" style="overflow:scroll">'+ JSON.stringify(terreno_obtenido) + '</div>  <script type="text/javascript">parent.App.terrain_selected = JSON.parse(document.getElementById(\'terrain_details_result\').innerHTML);parent.App.update_terrain_data();</script>';
        //   } else {
        //       html.defaultValue = JSON.stringify(responseResult)
        //   }

        // }

        section = 'Print form';
        {
          context.response.write(JSON.stringify(responseResult));
          // context.response.writePage(form);
        }

      }

    } catch (err) {
      throw err;
    }
  }

  function get_all_terrains() {
    var terrenosObj = {};
    var result = search.create({
      type: 'customrecord_efx_as_terreno',
      filters: [
        ['isinactive', search.Operator.IS, 'F']
      ],
      columns: [
        { name: 'name' }
      ]
    });

    var resultData = result.run();
    var start = 0;
    do {

      var resultSet = resultData.getRange(start, start + 1000);
      if (resultSet && resultSet.length > 0) {
        for (var i = 0; i < resultSet.length; i++) {
          var rId = resultSet[i].id;
          var rName = resultSet[i].getValue({ name: 'name' }) || '';
          if (!terrenosObj[rId]) {
            terrenosObj[rId] = {
              id: rId,
              name: rName
            };
          }
        }
      }

      start += 1000;

    } while (resultSet && resultSet.length == 1000);

    return terrenosObj;

  }

  function get_terrain_details(terrain_id) {
    var terreno = {};

    var result = search.create({
      type: 'customrecord_efx_as_terreno',
      filters: [
        ['isinactive', search.Operator.IS, 'F'], 'and',
        ['internalid', search.Operator.IS, terrain_id]
      ],
      columns: [
        { name: 'name' }
      ]
    });

    var resultData = result.run();
    var start = 0;
    do {

      var resultSet = resultData.getRange(start, start + 1000);
      if (resultSet && resultSet.length > 0) {
        for (var i = 0; i < resultSet.length; i++) {
          var rId = resultSet[0].id;
          var rName = resultSet[0].getValue({ name: 'name' }) || '';

          if (!terreno[rId]) {
            terreno[rId] = {
              id: rId,
              name: rName,
              parcelas: {}
            };
          }
        }
      }

      start += 1000;

    } while (resultSet && resultSet.length == 1000);

    log.audit({ title: 'Terrain details', details: JSON.stringify(terreno) });

    //get the parcelas
    var parcelas_search = search.create({
      type: 'customrecord_efx_as_unidad',
      filters: [
        ['isinactive', search.Operator.IS, 'F'], 'and',
        ['custrecord_efx_as_p_terreno', search.Operator.IS, terrain_id]
      ],
      columns: [
        { name: 'name' },
        { name: 'custrecord_efx_as_p_terreno' }
      ]
    });

    var resultData = parcelas_search.run();
    var start = 0;
    do {

      var resultSet = resultData.getRange(start, start + 1000);
      if (resultSet && resultSet.length > 0) {
        for (var i = 0; i < resultSet.length; i++) {
          var pId = resultSet[i].id;
          var tId = resultSet[i].getValue({ name: 'custrecord_efx_as_p_terreno' }) || '';
          var pName = resultSet[i].getValue({ name: 'name' }) || '';
          log.audit({ title: 'Parcelas actual', details: JSON.stringify(resultSet[i]) });

          if (terreno[tId]) {
            if (!terreno[tId].parcelas[pId]) {
              terreno[tId].parcelas[pId] = {
                id: pId,
                name: pName,
                terreno: tId
              }
            }
          }

        }
      }

      start += 1000;

    } while (resultSet && resultSet.length == 1000);
    log.audit({ title: 'Parcelas obtenidas', details: JSON.stringify(terreno) });
    //end get parcelas

    return terreno;
  }

  return {
    onRequest: onRequest
  };
});
