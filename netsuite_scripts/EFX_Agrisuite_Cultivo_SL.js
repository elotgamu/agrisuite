/*******************************************************************
* @NApiVersion 2.x
* @NScriptType Suitelet
* Name: EFX_Agrisuite_Cultivo_SL.js
*
* Author: Efficientix Dev Team
* Purpose: All stuuf related parcel custom record (CRUD)
* Script: The script record id
* Deploy: The script deployment record id
* ******************************************************************* */

define(['N/record', 'N/search', 'N/ui/serverWidget','N/format'], function (record, search, ui,format) {
    function onRequest(context) {
      var section = '';

      try {
          if (context.request.method == 'GET') {
              section = 'Get Parameters';
              {
                  //view
                  //create
                  //update
                  //delete
                  //

                  var custparam_plaga = context.request.parameters.custparam_plaga || '';
                  var custparam_fecha = context.request.parameters.custparam_fecha || '';

                  var custparam_mode = context.request.parameters.custparam_mode || '';
                  var custparam_id = context.request.parameters.custparam_id || '';
                  var custparam_name = context.request.parameters.custparam_name || '';
                  var custparam_planta=context.request.parameters.custparam_planta ||'';
                  var custparam_surco=context.request.parameters.custparam_surco ||'';
                  var custparam_densidad=context.request.parameters.custparam_densidad ||'';
                  var custparam_tipo_cultivo=context.request.parameters.custparam_tipo_cultivo ||'';
                  var custparam_tipo_variedad=context.request.parameters.custparam_tipo_variedad ||'';
                  var custparam_activo_productivo=context.request.parameters.custparam_activo_productivo ||'';
                    var custparam_tamano = context.request.parameters.custparam_tamano ||'';
                    var custparam_unidad_medida = context.request.parameters.custparam_unidadmedida || '';


                  var responseResult = {
                      statusCode: '',
                      message: '',
                      id: '',
                      name: '',
                      results: {}
                  };
              }
              switch(custparam_mode){
                  case 'dll_plaga':
                    var plagaAgregada = quitarPlaga(custparam_id,custparam_fecha);
                    if (plagaAgregada != custparam_id) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo quitar la plaga del cultivo';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha quitado la plaga del cultivo';
                        responseResult.results = plagaAgregada;
                    }
                break;

                  case 'add_plaga':
                    var plagaAgregada = agregarPlaga(custparam_id,custparam_plaga,custparam_fecha);
                    if (plagaAgregada != custparam_id) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo agregar plaga al cultivo';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha agregado plaga al cultivo';
                        responseResult.results = plagaAgregada;
                    }
                break;
                case 'view':
                    var cultivos_obtenidos = obtenerTodosCultivos();
                    if (Object.keys(cultivos_obtenidos).length == 0) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo obtener los cultivos de los surcos';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha obtenido las cultivos de los surcos';
                        responseResult.results = cultivos_obtenidos;
                    }
                break;
                case 'create'://idparcela,nombre tunel, tamaño, unidad
                    if (custparam_name&&custparam_planta&&custparam_surco&&custparam_densidad&&custparam_tipo_cultivo&&custparam_tipo_variedad&&custparam_tamano&&custparam_unidad_medida) {
                        section = 'Create cultivo';
                        {
                            var rec = record.create({
                                type: 'customrecord_efx_as_cultivo'
                            });

                            rec.setValue({
                                fieldId: 'name',
                                value: custparam_name
                            });

                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_surco',
                                value: custparam_surco
                            });

                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_planta',
                                value: custparam_planta
                            });

                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_densidad',
                                value: custparam_densidad
                            });
                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_tipo_cultivo',
                                value: custparam_tipo_cultivo
                            });

                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_tipo_variedad',
                                value: custparam_tipo_variedad
                            });
                            rec.setValue({
                                fieldId: 'custrecord_efx_as_c_act_prod',
                                value: custparam_activo_productivo
                            });

                            var recId = rec.save({
                                enableSourcing: true,
                                ignoreMandatoryFields: true
                            });

                            responseResult.statusCode = 2;
                            responseResult.message = 'Creado Exitosamente!';
                            responseResult.id = recId;
                            responseResult.name = custparam_name;
                            responseResult.results.name = custparam_name;
                            responseResult.results.id=recId;
                        }
                    }

                    else {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, Es necesario el parametro Nombre, Surco, Planta, Densidad, Tipo de Cultivo, Tipo de Variedad, Activo Productivo!';
                    }
                break;
                case 'detailview': //cultivo
                    // var cultivo_obtenido = obtenerPlanta(custparam_id);
                    var cultivo_obtenido = detalle_cultivo(custparam_id)
                    if (Object.keys(cultivo_obtenido).length == 0) {
                        responseResult.statusCode = 3;
                        responseResult.message = 'Error, NO se pudo obtener las planta del cultivos';
                    } else {
                        responseResult.statusCode = 2;
                        responseResult.message = 'Se ha obtenido la planta del cultivo';
                        responseResult.results = cultivo_obtenido;
                    }
                break;
                default:
                    responseResult.statusCode = 3;
                    responseResult.message = 'No se ha mandado una accion';
                    responseResult.results = {};
              };

              section = 'Print form';
              {
                  context.response.write(JSON.stringify(responseResult));
              }

          }

      } catch (err) {
          throw err;
      }
    }
    function obtenerTodosCultivos(){

        var objSurco = {};
        var result = search.create({
            type: 'customrecord_efx_as_cultivo',
            filters: [
                ['isinactive', search.Operator.IS, 'F'],
                'and',
                ['custrecord_efx_as_c_surco', search.Operator.NONEOF, '@NONE@']
            ],
            columns: [
                {name:'name'},
                {name :'custrecord_efx_as_c_planta'},
                {name :'custrecord_efx_as_c_surco'},
                {name :'custrecord_efx_as_c_densidad'},
                {name :'custrecord_efx_as_c_tipo_cultivo'},
                {name :'custrecord_efx_as_c_tipo_variedad'},
                {name :'custrecord_efx_as_c_act_prod'}
            ]
        });

        var resultData = result.run();
        var start = 0;
        do {
            var resultSet = resultData.getRange(start, start + 1000);
            if (resultSet && resultSet.length > 0) {
                for (var i = 0; i < resultSet.length; i++) {
                    var idCiltivo=resultSet[i].id;
                    var name =resultSet[i].getValue({name:'name'});


                    var surco=resultSet[i].getValue({name :'custrecord_efx_as_c_surco'});
                    var surcoName=resultSet[i].getText({name :'custrecord_efx_as_c_surco'});

                    var planta=resultSet[i].getValue({name :'custrecord_efx_as_c_planta'});
                    var densidad=resultSet[i].getValue({name :'custrecord_efx_as_c_densidad'});
                    var tipo_cultivo=resultSet[i].getValue({name :'custrecord_efx_as_c_tipo_cultivo'});
                    var tipo_variedad=resultSet[i].getValue({name :'custrecord_efx_as_c_tipo_variedad'});
                    var act_prod=resultSet[i].getValue({name :'custrecord_efx_as_c_act_prod'});

                    if(!objSurco[surco]){
                        objSurco[surco]={
                            id:tunel,
                            name:surcoName,
                            cultivo:{}
                        };
                    }
                    if(!objSurco[surco].cultivo[idCiltivo]){
                        objSurco[surco].cultivo[idCiltivo]={
                            id:idCiltivo,
                            name:name,
                            planta:planta,
                            densidad:densidad,
                            tipo_cultivo:tipo_cultivo,
                            tipo_variedad:tipo_variedad,
                            act_prod:act_prod
                        };
                    }


                }
            }
            start += 1000;

        } while (resultSet && resultSet.length == 1000);

        log.audit({ title: 'objSurco', details: JSON.stringify(objSurco) });
        return objSurco;
    }
    function arreglarFecha(fecha){
        //   yyyy-mm-dd
        //   MM/DD/YYYY
        // 05/21/2018

        log.audit({title:'fecha',details:fecha});
        var arrayfecha=fecha.split('-');
        var fechaString=arrayfecha[2]+'/'+arrayfecha[1]+'/'+arrayfecha[0];
        log.audit({title:'fechaString',details:fechaString});

        var fechaFormato=format.format({
            value:fechaString,
            type:format.Type.DATE
        });
        return fechaFormato;
    }
    function agregarPlaga(idparcela,typePlaga,fecha){

        var plagaAgregada=0;
        try{
            var fechaFormato=arreglarFecha(fecha);
            plagaAgregada = record.submitFields({
                type: 'customrecord_efx_as_cultivo',
                id: idparcela,
                values: {
                    'custrecord_efx_as_c_plaga': typePlaga,
                    'custrecord_efx_as_c_fechaplagainicio':fechaFormato
                }
            });
        }catch(errorModCultivo){
            log.audit({title:'errorModCultivo',details:errorModCultivo});
        }
        return plagaAgregada;
    }
    function quitarPlaga(idparcela,fecha){
        var plagaAgregada='';
        try{
            var fechaFormato=arreglarFecha(fecha);
            plagaAgregada = record.submitFields({
                type: 'customrecord_efx_as_cultivo',
                id: idparcela,
                values: {
                    'custrecord_efx_as_c_plaga': '',
                    'custrecord_efx_as_c_fechaplagafin':fechaFormato
                }
            });

        }catch(errorModCultivo){
            log.audit({title:'errorModCultivo',details:errorModCultivo});
        }

        return plagaAgregada;
    }
    function obtenerPlanta(idParamPlanta){

        var objPlanta = {};
        var result = search.create({
            type: 'customrecord_efx_as_cultivo',
            filters: [
                ['isinactive', search.Operator.IS, 'F'],
                'and',
                ['custrecord_efx_as_c_planta', search.Operator.ANYOF, idParamPlanta]

            ],
            columns: [
                {name:'custrecord_efx_as_c_planta'},
                {join :'custrecord_efx_as_c_planta',name:'custrecord_efx_as_p_tipovariedad'},
                {name:'custrecord_efx_as_c_densidad'},
                {name:'custrecord_efx_as_c_act_prod'},
                {name:'custrecord_efx_as_c_plaga'},
                {name:'custrecord_efx_as_c_fechaplagainicio'},
                {name:'custrecord_efx_as_c_fechaplagafin'}

            ]
        });


        var resultData = result.run();
        var start = 0;
        do {
            var resultSet = resultData.getRange(start, start + 1000);
            if (resultSet && resultSet.length > 0) {
                for (var i = 0; i < resultSet.length; i++) {
                    var idPlanta =resultSet[i].getValue({name:'custrecord_efx_as_c_planta'});
                    var name =resultSet[i].getText({name:'custrecord_efx_as_c_planta'});

                    var tipo_variedad=resultSet[i].getText({join :'custrecord_efx_as_c_planta',name:'custrecord_efx_as_p_tipovariedad'});

                    var densidad=resultSet[i].getValue({name:'custrecord_efx_as_c_densidad'});
                    var act_prod=resultSet[i].getValue({name:'custrecord_efx_as_c_act_prod'});
                    var plaga=resultSet[i].getValue({name:'custrecord_efx_as_c_plaga'});
                    var fechaplagainicio=resultSet[i].getValue({name:'custrecord_efx_as_c_fechaplagainicio'});
                    var fechaplagafin=resultSet[i].getValue({name:'custrecord_efx_as_c_fechaplagafin'});

                    if(!objPlanta[idPlanta]&&idPlanta){
                        objPlanta[idPlanta]={
                            id:idPlanta,
                            name:name,
                            tipo_variedad:tipo_variedad,
                            densidad:densidad,
                            act_prod:act_prod,
                            plaga:plaga,
                            fechaplagaini:fechaplagainicio,
                            fechaplagafin:fechaplagafin
                        };
                    }
                }
            }
            start += 1000;
        } while (resultSet && resultSet.length == 1000);

        log.audit({ title: 'objPlanta', details: JSON.stringify(objPlanta) });
        return objPlanta;
    }

    function detalle_cultivo(id_cultivo) {
      var CultivoObj = {
        id: null,
        name: null,
        densidad: null,
        id_tipo_variedad: null,
        tipo_variedad: null,
        activoProd: null,
        planta: null,
        id_planta: null,
        plaga: null,
        id_plaga: null,
        fecha_inicio: null,
        fecha_fin: null
      };
      var result = search.create({
          type: 'customrecord_efx_as_cultivo',
          filters: [
              ['isinactive', search.Operator.IS, 'F'],
              'and',
              ['internalid', search.Operator.IS, id_cultivo]

          ],
          columns: [
              {name:'name'},
              {name :'custrecord_efx_as_c_planta'},
              {name:'custrecord_efx_as_c_densidad'},
              {name:'custrecord_efx_as_c_act_prod'},
              {name:'custrecord_efx_as_c_plaga'},
              {name:'custrecord_efx_as_c_fechaplagainicio'},
              {name:'custrecord_efx_as_c_fechaplagafin'},
              { name: 'custrecord_efx_as_c_tipo_variedad' }
          ]
      });

      var resultData = result.run();
      var start = 0;
      do {
          var resultSet = resultData.getRange(start, start + 1000);
          if (resultSet && resultSet.length > 0) {
              for (var i = 0; i < resultSet.length; i++) {
                  // var idPlanta =resultSet[i].getValue({name:'custrecord_efx_as_c_planta'});
                  // var name =resultSet[i].getText({name:'custrecord_efx_as_c_planta'});
                  var id = resultSet[i].id;
                  var name = resultSet[i].getValue({name:'name'});

                  var tipo_variedad = resultSet[i].getText({name:'custrecord_efx_as_c_tipo_variedad'});
                  var id_tipo_variedad = resultSet[i].getValue({name:'custrecord_efx_as_c_tipo_variedad'});
                  var planta_id = resultSet[i].getValue({name:'custrecord_efx_as_c_planta'});
                  var planta_name = resultSet[i].getText({name:'custrecord_efx_as_c_planta'});
                  var densidad = resultSet[i].getValue({name:'custrecord_efx_as_c_densidad'});
                  var act_prod = resultSet[i].getValue({name:'custrecord_efx_as_c_act_prod'});
                  var id_plaga = resultSet[i].getValue({name:'custrecord_efx_as_c_plaga'});
                  var plaga = resultSet[i].getText({name:'custrecord_efx_as_c_plaga'});
                  var fechaplagainicio=resultSet[i].getValue({name:'custrecord_efx_as_c_fechaplagainicio'});
                  var fechaplagafin=resultSet[i].getValue({name:'custrecord_efx_as_c_fechaplagafin'});

                  // if(!objPlanta[idPlanta]&&idPlanta){
                  //     objPlanta[idPlanta]={
                  //         id:idPlanta,
                  //         name:name,
                  //         tipo_variedad:tipo_variedad,
                  //         densidad:densidad,
                  //         act_prod:act_prod,
                  //         plaga:plaga,
                  //         fechaplagaini:fechaplagainicio,
                  //         fechaplagafin:fechaplagafin
                  //     };
                  // }
                  CultivoObj.id = id;
                  CultivoObj.name = name;
                  CultivoObj.densidad = densidad;
                  CultivoObj.activoProd = act_prod;
                  CultivoObj.fecha_inicio = fechaplagainicio;
                  CultivoObj.fecha_fin = fechaplagafin;
                  CultivoObj.id_plaga = id_plaga;
                  CultivoObj.plaga = plaga;
                  CultivoObj.tipo_variedad = tipo_variedad;
                  CultivoObj.id_tipo_variedad = id_tipo_variedad;
                  CultivoObj.planta = planta_name
                  CultivoObj.id_planta = planta_id


                  log.audit({title: 'Resultado en ciclo', details: JSON.stringify(resultSet[i])});

              }
          }
          start += 1000;
      } while (resultSet && resultSet.length == 1000);

      log.audit({title: 'Cultivo obtenido ', details: JSON.stringify(CultivoObj)})
      return CultivoObj;
    }

    return {
      onRequest: onRequest
    };
  });
