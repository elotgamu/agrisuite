'use strict'

//activating tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

function server_callbacks(script, deploy) {
  // body...
  let action_string = {
    start: `require(['N/https', 'N/url', 'N/currentRecord', 'N/format'], function(https, urlMod, currentRecord, format) {
            var rec = currentRecord.get();
            var frame_url = urlMod.resolveScript({
              scriptId: '${script}',
              deploymentId: '${deploy}',
              returnExternalUrl: false
            });`,
    end: `var responseObject = https.request({
            method: https.Method.POST
            url: frame_url,
          });
          var parsed_response = JSON.parse(responseObject.body);
          document.getElementById('response-result-units').innerHTML = JSON.stringify(parsed_response);
          App.response_result = JSON.stringify(parsed_response);`,
    close: '});'
  }
  return action_string
}

function get_units_info() {
  //This gets all the units for parcel parcel_creation
  let unit_action_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  unit_action_string += 'var rec = currentRecord.get();';
  // add_terrain_action += 'debugger;';
  unit_action_string += 'var frame_url = urlMod.resolveScript({';
  unit_action_string += 'scriptId: \'customscript_efx_as_parcela_sl\',';
  unit_action_string += 'deploymentId: \'customdeploy_efx_as_parcela_sl\',';
  unit_action_string += 'returnExternalUrl: false';
  unit_action_string += '});';

  unit_action_string += 'var frame_url = frame_url + \'&custparam_mode=unit\';';
  unit_action_string += 'var responseObject = https.get({';
  unit_action_string += ' url: frame_url';
  unit_action_string += '});'
  unit_action_string += 'var parsed_response = JSON.parse(responseObject.body);';
  unit_action_string += 'document.getElementById(\'response-result-units\').innerHTML = JSON.stringify(parsed_response);';
  // add_terrain_action += 'var response = JSON.parse(document.getElementById(\'response_result\').innerHTML);';
  unit_action_string += 'App.pass_parcel_units();';
  unit_action_string += '});';
  let btn_get_units = document.getElementById('btn-get-units')
  btn_get_units.setAttribute('onclick', unit_action_string)
  btn_get_units.click()
}

function get_plant_list() {
  let plant_list_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  plant_list_string += 'var rec = currentRecord.get();';
  plant_list_string += 'var frame_url = urlMod.resolveScript({';
  plant_list_string += 'scriptId: \'customscript_efx_as_planta_sl\',';
  plant_list_string += 'deploymentId: \'customdeploy_efx_as_planta_sl\',';
  plant_list_string += 'returnExternalUrl: false';
  plant_list_string += '});';

  plant_list_string += 'var frame_url = frame_url + \'&custparam_mode=view\';';
  plant_list_string += 'var responseObject = https.get({';
  plant_list_string += ' url: frame_url';
  plant_list_string += '});';
  plant_list_string += 'var parsed_response = JSON.parse(responseObject.body);';
  plant_list_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  // add_terrain_action += 'var response = JSON.parse(document.getElementById(\'response_result\').innerHTML);';
  plant_list_string += 'App.pass_plant_list();';
  plant_list_string += '});';
  let btn_get_plants = document.getElementById('btn-get-plants')
  btn_get_plants.setAttribute('onclick', plant_list_string)
  btn_get_plants.click()
}

function get_plages() {
  let plages_types_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  plages_types_string += 'var rec = currentRecord.get();';
  plages_types_string += 'var frame_url = urlMod.resolveScript({';
  plages_types_string += 'scriptId: \'customscript_efx_as_unit_sl\',';
  plages_types_string += 'deploymentId: \'customdeploy_efx_as_unit_sl\',';
  plages_types_string += 'returnExternalUrl: false';
  plages_types_string += '});';

  plages_types_string += 'var frame_url = frame_url + \'&custparam_mode=plagas\';';
  plages_types_string += 'var responseObject = https.get({';
  plages_types_string += ' url: frame_url';
  plages_types_string += '});';
  plages_types_string += 'var parsed_response = JSON.parse(responseObject.body);';
  plages_types_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  plages_types_string += 'App.pass_plagues();';
  plages_types_string += '});';
  let btn_get_plages = document.getElementById('btn-get-plages')
  btn_get_plages.setAttribute('onclick', plages_types_string)
  btn_get_plages.click()
}


function add_plague_to_zone(zone_id, plague, date_detected) {
  let plague_addition_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  plague_addition_string += 'var rec = currentRecord.get();';
  plague_addition_string += 'var frame_url = urlMod.resolveScript({';
  plague_addition_string += 'scriptId: \'customscript_efx_as_cultivo_sl\',';
  plague_addition_string += 'deploymentId: \'customdeploy_efx_as_cultivo_sl\',';
  plague_addition_string += 'returnExternalUrl: false';
  plague_addition_string += '});';
  plague_addition_string += 'var frame_url = frame_url + \'&custparam_mode=add_plaga&custparam_plaga=\'+\'' + plague + '\'+  \'&custparam_fecha=\'+\'' + date_detected +'\'+ \'&custparam_id=\'+\'' + zone_id + '\';';
  plague_addition_string += 'var responseObject = https.get({';
  plague_addition_string += ' url: frame_url';
  plague_addition_string += '});';
  plague_addition_string += 'var parsed_response = JSON.parse(responseObject.body);';
  plague_addition_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  plague_addition_string += 'App.plague_to_crop('+ zone_id +');';
  plague_addition_string += '});';
  let btn_add_crop_plague = document.getElementById('btn-plague-to-crop')
  btn_add_crop_plague.setAttribute('onclick', plague_addition_string)
  btn_add_crop_plague.click()
}

function control_plague_on_zone(zone_id, plague, date_controled) {
  let plague_controlled_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  plague_controlled_string += 'var rec = currentRecord.get();';
  plague_controlled_string += 'var frame_url = urlMod.resolveScript({';
  plague_controlled_string += 'scriptId: \'customscript_efx_as_cultivo_sl\',';
  plague_controlled_string += 'deploymentId: \'customdeploy_efx_as_cultivo_sl\',';
  plague_controlled_string += 'returnExternalUrl: false';
  plague_controlled_string += '});';
  plague_controlled_string += 'var frame_url = frame_url + \'&custparam_mode=dll_plaga&custparam_plaga=\'+\'' + plague + '\'+  \'&custparam_fecha=\'+\'' + date_controled +'\'+ \'&custparam_id=\'+\'' + zone_id + '\';';
  plague_controlled_string += 'var responseObject = https.get({';
  plague_controlled_string += ' url: frame_url';
  plague_controlled_string += '});';
  plague_controlled_string += 'var parsed_response = JSON.parse(responseObject.body);';
  plague_controlled_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  plague_controlled_string += 'App.plague_to_crop('+ zone_id +');';
  plague_controlled_string += '});';
  let btn_ctrl_plague = document.getElementById('btn-control-plague')
  btn_ctrl_plague.setAttribute('onclick', plague_controlled_string)
  btn_ctrl_plague.click()
}

function create_plant(name) {
  let plant_create_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  plant_create_string += 'var rec = currentRecord.get();';
  plant_create_string += 'var frame_url = urlMod.resolveScript({';
  plant_create_string += 'scriptId: \'customscript_efx_as_planta_sl\',';
  plant_create_string += 'deploymentId: \'customdeploy_efx_as_planta_sl\',';
  plant_create_string += 'returnExternalUrl: false';
  plant_create_string += '});';
  plant_create_string += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_name=\'+\'' + name + '\';';
  plant_create_string += 'var responseObject = https.get({';
  plant_create_string += ' url: frame_url';
  plant_create_string += '});';
  plant_create_string += 'var parsed_response = JSON.parse(responseObject.body);';
  plant_create_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  plant_create_string += 'App.get_created_plant();';
  plant_create_string += '});';

  let btn_create_plant= document.getElementById('btn-create-planta')
  btn_create_plant.setAttribute('onclick', plant_create_string)
  btn_create_plant.click()
}

function get_crop_type() {
  let crop_types_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  crop_types_string += 'var rec = currentRecord.get();';
  crop_types_string += 'var frame_url = urlMod.resolveScript({';
  crop_types_string += 'scriptId: \'customscript_efx_as_unit_sl\',';
  crop_types_string += 'deploymentId: \'customdeploy_efx_as_unit_sl\',';
  crop_types_string += 'returnExternalUrl: false';
  crop_types_string += '});';

  crop_types_string += 'var frame_url = frame_url + \'&custparam_mode=cultivo\';';
  crop_types_string += 'var responseObject = https.get({';
  crop_types_string += ' url: frame_url';
  crop_types_string += '});';
  crop_types_string += 'var parsed_response = JSON.parse(responseObject.body);';
  crop_types_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  crop_types_string += 'App.pass_crop_types();';
  crop_types_string += '});';
  let btn_crop_type = document.getElementById('btn-get-crop-types')
  btn_crop_type.setAttribute('onclick', crop_types_string)
  btn_crop_type.click()
}

function get_crop_type_variety() {
  let crop_t_variety_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  crop_t_variety_string += 'var rec = currentRecord.get();';
  crop_t_variety_string += 'var frame_url = urlMod.resolveScript({';
  crop_t_variety_string += 'scriptId: \'customscript_efx_as_unit_sl\',';
  crop_t_variety_string += 'deploymentId: \'customdeploy_efx_as_unit_sl\',';
  crop_t_variety_string += 'returnExternalUrl: false';
  crop_t_variety_string += '});';

  crop_t_variety_string += 'var frame_url = frame_url + \'&custparam_mode=variedad\';';
  crop_t_variety_string += 'var responseObject = https.get({';
  crop_t_variety_string += ' url: frame_url';
  crop_t_variety_string += '});';
  crop_t_variety_string += 'var parsed_response = JSON.parse(responseObject.body);';
  crop_t_variety_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  crop_t_variety_string += 'App.pass_crop_variety_types();';
  crop_t_variety_string += '});';
  let btn_variety_type = document.getElementById('btn-get-crop-variety-types')
  btn_variety_type.setAttribute('onclick', crop_t_variety_string)
  btn_variety_type.click()
}

function add_new_terrain() {
  let new_terrain_name = document.getElementById('terrain-name-field').value;
  let add_terrain_action = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';

  add_terrain_action += 'var rec = currentRecord.get();';
  // add_terrain_action += 'debugger;';
  add_terrain_action += 'var frame_url = urlMod.resolveScript({';
  add_terrain_action += 'scriptId: \'customscript_efx_as_terreno_sl\',';
  add_terrain_action += 'deploymentId: \'customdeploy_efx_as_terreno_sl\',';
  add_terrain_action += 'returnExternalUrl: false';
  add_terrain_action += '});';
  add_terrain_action += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_terreno=\'+\'' + new_terrain_name + '\';';
  add_terrain_action += 'var responseObject = https.get({';
  add_terrain_action += ' url: frame_url';
  add_terrain_action += '});';
  add_terrain_action += 'var parsed_response = JSON.parse(responseObject.body);';
  add_terrain_action += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  // add_terrain_action += 'var response = JSON.parse(document.getElementById(\'response_result\').innerHTML);';
  add_terrain_action += 'App.get_created_terrain();';
  add_terrain_action += '});';
  let btn_new_terrain = document.getElementById('btn-add-terrain');
  btn_new_terrain.setAttribute('onclick', add_terrain_action)
  $('#modal-new-terrain').modal('hide')
  $('#modal-new-terrain').find('form').trigger('reset')
  btn_new_terrain.click();
}

function get_terrain_details(id) {
  let details_action = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  details_action += 'var rec = currentRecord.get();';
  // add_terrain_action += 'debugger;';
  details_action += 'var frame_url = urlMod.resolveScript({';
  details_action += 'scriptId: \'customscript_efx_as_terreno_sl\',';
  details_action += 'deploymentId: \'customdeploy_efx_as_terreno_sl\',';
  details_action += 'returnExternalUrl: false';
  details_action += '});';
  details_action += 'var frame_url = frame_url + \'&custparam_mode=detailview&custparam_id=\'+\'' + id + '\';';
  details_action += 'var responseObject = https.get({';
  details_action += ' url: frame_url';
  details_action += '});';
  details_action += 'var parsed_response = JSON.parse(responseObject.body);';
  details_action += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  details_action += 'App.update_terrain_data();';
  details_action += '});';

  let btn_view_terrain = document.getElementById('btn-detail-terrain');
  btn_view_terrain.setAttribute('onclick', details_action)
  btn_view_terrain.click();
}

function create_parcel(name, size, unit_id, terrain_id, parcel_unit_id) {
  let parcel_creation_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  parcel_creation_string += 'var rec = currentRecord.get();';
  parcel_creation_string += 'var frame_url = urlMod.resolveScript({';
  parcel_creation_string += 'scriptId: \'customscript_efx_as_parcela_sl\',';
  parcel_creation_string += 'deploymentId: \'customdeploy_efx_as_parcela_sl\',';
  parcel_creation_string += 'returnExternalUrl: false';
  parcel_creation_string += '});';

  parcel_creation_string += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_name=\'+\'' + name + '\'+ \'&custparam_size=\'+\'' + size + '\'+ \'&custparam_unit=\'+\'' + unit_id + '\'+ \'&custparam_terreno=\'+\'' + terrain_id + '\'+ \'&custparam_parcel_unit=\'+\'' + parcel_unit_id +'\';';

  parcel_creation_string += 'var responseObject = https.get({';
  parcel_creation_string += ' url: frame_url';
  parcel_creation_string += '});';
  parcel_creation_string += 'var parsed_response = JSON.parse(responseObject.body);';
  parcel_creation_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  parcel_creation_string += 'App.get_created_parcel('+ parcel_unit_id +' , '+ terrain_id +');';
  parcel_creation_string += '});';
  let btn_add_parcela = document.getElementById('btn-add-parcela');
  btn_add_parcela.setAttribute('onclick', parcel_creation_string);
  $('#modal-new-parcel').modal('hide');
  btn_add_parcela.click();
}

function get_parcel_details(id) {
  let parcel_details_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  parcel_details_string += 'var rec = currentRecord.get();';
  parcel_details_string += 'var frame_url = urlMod.resolveScript({';
  parcel_details_string += 'scriptId: \'customscript_efx_as_parcela_sl\',';
  parcel_details_string += 'deploymentId: \'customdeploy_efx_as_parcela_sl\',';
  parcel_details_string += 'returnExternalUrl: false';
  parcel_details_string += '});';
  parcel_details_string += 'var frame_url = frame_url + \'&custparam_mode=detailview&custparam_id=\'+\'' + id + '\';';
  parcel_details_string += 'var responseObject = https.get({';
  parcel_details_string += ' url: frame_url';
  parcel_details_string += '});';
  parcel_details_string += 'var parsed_response = JSON.parse(responseObject.body);';
  parcel_details_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  parcel_details_string += 'App.pass_parcel_details();';
  parcel_details_string += '});';
  let btn_view_parcela = document.getElementById('btn-detail-parcela');
  btn_view_parcela.setAttribute('onclick', parcel_details_string);
  btn_view_parcela.click();
}

function get_all_tunnels() {
  let tunel_list_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  tunel_list_string += 'var rec = currentRecord.get();';
  tunel_list_string += 'var frame_url = urlMod.resolveScript({';
  tunel_list_string += 'scriptId: \'customscript_efx_as_tunel\',';
  tunel_list_string += 'deploymentId: \'customdeploy_efx_as_tunel\',';
  tunel_list_string += 'returnExternalUrl: false';
  tunel_list_string += '});';
  tunel_list_string += 'var frame_url = frame_url + \'&custparam_mode=view\';';
  tunel_list_string += 'var responseObject = https.get({';
  tunel_list_string += ' url: frame_url';
  tunel_list_string += '});';
  tunel_list_string += 'var parsed_response = JSON.parse(responseObject.body);';
  tunel_list_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  tunel_list_string += 'App.pass_tunnels_list();';
  tunel_list_string += '});';

  let btn_tunnel_list = document.getElementById('btn-list-tunel')
  btn_tunnel_list.setAttribute('onclick', tunel_list_string)
  btn_tunnel_list.click();
}

function get_tunel_data(tunnel_id) {
  let tunel_detail_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  tunel_detail_string += 'var rec = currentRecord.get();';
  tunel_detail_string += 'var frame_url = urlMod.resolveScript({';
  tunel_detail_string += 'scriptId: \'customscript_efx_as_tunel\',';
  tunel_detail_string += 'deploymentId: \'customdeploy_efx_as_tunel\',';
  tunel_detail_string += 'returnExternalUrl: false';
  tunel_detail_string += '});';
  tunel_detail_string += 'var frame_url = frame_url + \'&custparam_mode=detailview&custparam_id=\'+\'' + tunnel_id + '\';';
  tunel_detail_string += 'var responseObject = https.get({';
  tunel_detail_string += ' url: frame_url';
  tunel_detail_string += '});';
  tunel_detail_string += 'var parsed_response = JSON.parse(responseObject.body);';
  tunel_detail_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  tunel_detail_string += 'App.pass_tunnel_data();';
  tunel_detail_string += '});';

  let btn_tunnel_detail = document.getElementById('btn-detail-tunel')
  btn_tunnel_detail.setAttribute('onclick', tunel_detail_string)
  btn_tunnel_detail.click();
}

function create_tunnel(name, size, measure_unit, parcela) {
  let tunnel_create_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  tunnel_create_string += 'var rec = currentRecord.get();';
  tunnel_create_string += 'var frame_url = urlMod.resolveScript({';
  tunnel_create_string += 'scriptId: \'customscript_efx_as_tunel\',';
  tunnel_create_string += 'deploymentId: \'customdeploy_efx_as_tunel\',';
  tunnel_create_string += 'returnExternalUrl: false';
  tunnel_create_string += '});';
  tunnel_create_string += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_name=\'+\'' + name + '\'+ \'&custparam_size=\'+\'' + size + '\'+ \'&custparam_unit=\'+\'' + measure_unit + '\'+ \'&custparam_parcela=\'+\'' + parcela + '\';';
  tunnel_create_string += 'var responseObject = https.get({';
  tunnel_create_string += ' url: frame_url';
  tunnel_create_string += '});';
  tunnel_create_string += 'var parsed_response = JSON.parse(responseObject.body);';
  tunnel_create_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  tunnel_create_string += 'App.get_created_tunnel('+ parcela +');';
  tunnel_create_string += '});';
  let btn_create_tunnel = document.getElementById('btn-add-tunel')
  btn_create_tunnel.setAttribute('onclick', tunnel_create_string)
  btn_create_tunnel.click()
}

function create_groove(name, size, measure_unit, tunnel) {
  let groove_create_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  groove_create_string += 'var rec = currentRecord.get();';
  groove_create_string += 'var frame_url = urlMod.resolveScript({';
  groove_create_string += 'scriptId: \'customscript_efx_as_surco_sl\',';
  groove_create_string += 'deploymentId: \'customdeploy_efx_as_surco_sl\',';
  groove_create_string += 'returnExternalUrl: false';
  groove_create_string += '});';
  groove_create_string += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_name=\'+\'' + name + '\'+ \'&custparam_size=\'+\'' + size + '\'+ \'&custparam_unit=\'+\'' + measure_unit + '\'+ \'&custparam_tunel=\'+\'' + tunnel + '\';';
  groove_create_string += 'var responseObject = https.get({';
  groove_create_string += ' url: frame_url';
  groove_create_string += '});';
  groove_create_string += 'var parsed_response = JSON.parse(responseObject.body);';
  groove_create_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  groove_create_string += 'App.get_created_groove('+ tunnel +');';
  groove_create_string += '});';
  let btn_create_groove = document.getElementById('btn-create-surco')
  btn_create_groove.setAttribute('onclick', groove_create_string)
  btn_create_groove.click()
}

function get_groove_details(groove_id) {
  let groove_detail_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  groove_detail_string += 'var rec = currentRecord.get();';
  groove_detail_string += 'var frame_url = urlMod.resolveScript({';
  groove_detail_string += 'scriptId: \'customscript_efx_as_surco_sl\',';
  groove_detail_string += 'deploymentId: \'customdeploy_efx_as_surco_sl\',';
  groove_detail_string += 'returnExternalUrl: false';
  groove_detail_string += '});';
  groove_detail_string += 'var frame_url = frame_url + \'&custparam_mode=detailview&custparam_id=\'+\'' + groove_id + '\';';
  groove_detail_string += 'var responseObject = https.get({';
  groove_detail_string += ' url: frame_url';
  groove_detail_string += '});';
  groove_detail_string += 'var parsed_response = JSON.parse(responseObject.body);';
  groove_detail_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  groove_detail_string += 'App.pass_groove_data();';
  groove_detail_string += '});';
  let btn_groove_details = document.getElementById('btn-detalle-surco')
  btn_groove_details.setAttribute('onclick', groove_detail_string)
  btn_groove_details.click()
}

function create_crop(name, density, plant, crop_type, variety_type, size, measure_unit, groove) {
  let crop_creation_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  crop_creation_string += 'var rec = currentRecord.get();';
  crop_creation_string += 'var frame_url = urlMod.resolveScript({';
  crop_creation_string += 'scriptId: \'customscript_efx_as_cultivo_sl\',';
  crop_creation_string += 'deploymentId: \'customdeploy_efx_as_cultivo_sl\',';
  crop_creation_string += 'returnExternalUrl: false';
  crop_creation_string += '});';
  crop_creation_string += 'var frame_url = frame_url + \'&custparam_mode=create&custparam_name=\'+\'' + name + '\'+ \'&custparam_densidad=\'+\'' + density + '\'+ \'&custparam_planta=\'+\'' + plant + '\'+ \'&custparam_tipo_cultivo=\'+\'' + crop_type + '\'+ \'&custparam_tipo_variedad=\'+\'' + variety_type + '\'+ \'&custparam_tamano=\'+\'' + size + '\'+ \'&custparam_unidadmedida=\'+\'' + measure_unit + '\'+ \'&custparam_surco=\'+\''+ groove + '\';';
  crop_creation_string += 'var responseObject = https.get({';
  crop_creation_string += ' url: frame_url';
  crop_creation_string += '});';
  crop_creation_string += 'var parsed_response = JSON.parse(responseObject.body);';
  crop_creation_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  crop_creation_string += 'App.get_created_crop(' + groove + ');';
  crop_creation_string += '});';
  let btn_create_crop = document.getElementById('btn-create-crop')
  btn_create_crop.setAttribute('onclick', crop_creation_string)
  btn_create_crop.click()
}

function get_crop_details(crop_id) {
  let crop_detail_string = 'require([\'N/https\', \'N/url\', \'N/currentRecord\', \'N/format\'], function(https, urlMod, currentRecord, format) {';
  crop_detail_string += 'var rec = currentRecord.get();';
  crop_detail_string += 'var frame_url = urlMod.resolveScript({';
  crop_detail_string += 'scriptId: \'customscript_efx_as_cultivo_sl\',';
  crop_detail_string += 'deploymentId: \'customdeploy_efx_as_cultivo_sl\',';
  crop_detail_string += 'returnExternalUrl: false';
  crop_detail_string += '});';
  crop_detail_string += 'var frame_url = frame_url + \'&custparam_mode=detailview&custparam_id=\'+\'' + crop_id + '\';';
  crop_detail_string += 'var responseObject = https.get({';
  crop_detail_string += ' url: frame_url';
  crop_detail_string += '});';
  crop_detail_string += 'console.log(JSON.stringify(responseObject));';
  crop_detail_string += 'console.log(responseObject);';
  crop_detail_string += 'var parsed_response = JSON.parse(responseObject.body);';
  crop_detail_string += 'document.getElementById(\'response_result\').innerHTML = JSON.stringify(parsed_response);';
  crop_detail_string += 'App.pass_crop_data();';
  crop_detail_string += '});';
  let btn_crop_details = document.getElementById('btn-detalle-crop')
  btn_crop_details.setAttribute('onclick', crop_detail_string)
  btn_crop_details.click()
}

// function create_active_prod(crop_id, asset) {
//   const script_id = 'customscript_efx_as_activo_productivo_sl'
//   const deploy_id = 'customdeploy_efx_as_activo_productivo_sl'
//   const response_action = 'App.get_created_actv();'
//   let asset_string = JSON.stringify(asset)
//   let server_string = server_callbacks(script_id, deploy_id)
//   let params = `'&custparam_mode=create&custparam_cultivo=${crop_id}&custparam_activo=${asset_string}`
//   let server_call = server_string.start + params + server_string.end + response_action + server_string.close
//   console.log(server_call)
//   let btn_create_act_prod = document.getElementById('btn-create_actv-prod')
//   btn_create_act_prod.setAttribute('onclick', server_call)
//   btn_create_act_prod.click()
// }

function create_active_prod(crop_id, asset) {
  const script_id = 'customscript_efx_as_activo_productivo_sl'
  const deploy_id = 'customdeploy_efx_as_activo_productivo_sl'
  const response_action = 'App.get_created_actv();'
  const response = 'document.getElementById(\'response_result\').innerHTML = frame_url;App.get_created_actv();'
  let asset_string = JSON.stringify(asset)
  let server_string = server_callbacks(script_id, deploy_id)
  let params = '\'&custparam_mode=create\';'
  let server_call = server_string.start + params + response + server_string.close
  console.log(server_call)
  let btn_create_act_prod = document.getElementById('btn-create_actv-prod')
  btn_create_act_prod.setAttribute('onclick', server_call)
  btn_create_act_prod.click()
}

function get_script_url(crop, actv) {
  const script_id = 'customscript_efx_as_activo_productivo_sl'
  const deploy_id = 'customdeploy_efx_as_activo_productivo_sl'
  const response_action = 'App.get_created_actv();'
  const action_response = 'document.getElementById(\'response_result\').innerHTML = frame_url;App.create_actv('+ JSON.stringify(actv) +')'
  // let asset_string = JSON.stringify(asset)
  let server_string = server_callbacks(script_id, deploy_id)
  // let params = '\'&custparam_mode=create_act_prod\';'
  let server_call = server_string.start + action_response + server_string.close
  console.log(server_call)
  let btn_create_act_prod = document.getElementById('btn-create_actv-prod')
  btn_create_act_prod.setAttribute('onclick', server_call)
  btn_create_act_prod.click()
}

function get_month_range(start_date, months) {
  /*
  getting the months range between start date and number of months
  using momentjs library
   */

  let moment_start_date = moment(start_date)
  let moment_end_date = moment_start_date.clone().add(parseInt(months) - 1, 'months')
  let moment_end_date_string = moment_end_date.format('YYYY-MM-DD')
  let date_range = []

  while (moment_end_date > moment_start_date || moment_start_date.format('M') === moment_end_date.format('M')) {
   date_range.push(moment_start_date.format('DD/MM/YYYY'));
   moment_start_date.add(1,'month');
 }
  console.log(date_range)
  return date_range
}

function format_date(date) {
  /*
  Format the data to ouput like DD-MM-YYYY
   */
  let formated_date = moment(date).format('DD/MM/YYYY')
  return formated_date
}


//the Vue instance
const productive_asset = {
  template: '#asset-creation',
  props: ['asset', 'crop'],
  data: () => {
    return {
      message: 'Detalle activo productivo',
      prod_asset: null,
      loading: false,
      error: false,
      url: null,
      request_data: {
        mode: '',
        cultivo: null,
        activo: null,
        detalle_activo: null
      },
      response_data: null
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('url_obtained', (data) => {
      this.url = data
      this.create_asset()
    })
    this.initialize_asset()
    // this.setup_range()
  },
  methods: {
    initialize_asset() {
      this.loading = true
      setTimeout(() => {
        this.prod_asset = this.asset
        this.loading = false
        this.setup_range()
        this.prod_asset.date = format_date(this.prod_asset.date)
      }, 2500)
    },
    setup_range() {
      console.log(JSON.stringify(this.prod_asset))
      if (this.prod_asset) {
        let range = get_month_range(this.prod_asset.date, this.prod_asset.months)
        let actv_detail = {}
        range.forEach((date) => {
          actv_detail[date] = {
            month: date,
            curve_percent: null,
            budget_sp: null,
            est_box: null,
            est_cost: null
          }
        })
        this.prod_asset.actv_detail = actv_detail
      }
    },
    send_create_actv() {
      this.prod_asset.boxes, this.prod_asset.est_cost = 0
      for (var date in this.prod_asset.actv_detail) {
        this.prod_asset.boxes += parseInt(this.prod_asset.actv_detail[date].est_box) || 0
        this.prod_asset.est_cost += parseInt(this.prod_asset.actv_detail[date].est_cost) || 0
      }
      get_script_url(this.crop, this.prod_asset);
    },
    create_asset(){

      if (this.url) {
        this.request_data.mode = 'create_actv'
        this.request_data.cultivo = this.crop
        this.request_data.activo = this.prod_asset
        this.request_data.detalle_activo = this.prod_asset.actv_detail
      }

      if (this.url && this.request_data) {
        let data = JSON.stringify(this.request_data)

        let component_instance = this
        //we need to reasign the component instance since
        //'this' inside Axios will not refer to the actual
        //component


        axios.post(this.url, data, {
          headers: {
              'Content-Type': 'application/json',
          }
        }).then(function (response) {

          console.log(response);
          // alert(JSON.stringify(response.data))
          let server_response_data = response.data
          component_instance.response_data = server_response_data

          if ( (response.status == '200') && (component_instance.response_data.statusCode == '2') ) {
            alert(component_instance.response_data.message)
            component_instance.asset_success_created(component_instance.response_data)

          } else {
            alert('Asset not created...')
          }

        })
        .catch(function (error) {
          alert(error)
          console.log(error);
        });


      }

    },
    asset_success_created(data) {
      //pending to define what to do
    },
    asset_failed_created() {

    }
  }
}

const plants = {
  template: '#plant-list-view',
  props: ['plants'],
  data: () => {
    return {
      message: 'Listado de Plantas',
      plants_list: null,
      loading: false,
      empty: false,
      error: false,
      new_plant: {
        name: null
      }
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('plan_list_populated', (data) => {
      this.plants_list = data
      this.loading = false

      if( Object.keys(this.plants_list).length == 0 ) {
        this.empty = true
      } else {
        this.empty = false
      }
    })
    this.fetch_plants()
  },
  methods: {
    fetch_plants() {
      this.loading = true
      EventBus.$emit('fetch_plants_signal')
    },
    create_new_plant() {
      create_plant(this.new_plant.name)
      $('#modal-new-plant').modal('hide')
      this.clean_fields()
    },
    clean_fields() {
      this.new_plant.name = ''
    }
  }
}

const crop_details = {
  template: '#cultivo-details',
  props: ['id', 'name'],
  data: () => {
    return {
      message: 'Detalle de cultivo',
      loading: false,
      empty: false,
      crop: null,
      plagues: null,
      response_result: null,
      new_plague: {
        id: null,
        date_reported: null
      },
      plague_killed: {
        id: null,
        date_finished: null
      },
      new_activo: {
        name: null,
        date: null,
        months: null,
        curve: 100,
        sp_budget: null,
        boxes: null,
        est_cost: null,
        actv_detail: null
      }
    }
  },
  computed: {
    can_report_plague: function can_report_plague() {
      let can_report = true
      if ( (this.crop.fecha_inicio == '') && (this.crop.fecha_fin == '')  ) {
        can_report = false
      }
      if (this.crop.fecha_inicio != '' && this.crop.fecha_fin != '') {
        can_report = false
      }
      return can_report
    },
    can_control_plague: function can_control_plague() {
      let can_control = true
      if ( (this.crop.fecha_inicio == '') && (this.crop.fecha_fin == '')  ) {
        can_control = true
      }
      if ( (this.crop.fecha_fin == '') && (this.crop.fecha_inicio != '')) {
        can_control = false
      }
      return can_control
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('crop_details_fetched', (data) => {
      this.crop = data
      this.loading = false
      this.empty = false
    })
    EventBus.$on('plagues_fetched', (data) => {
      this.plagues = data
    })
    EventBus.$on('Created active prod', (data) => {
      this.response_result = data
    })
    this.fetch_crop_data(this.id)
    this.fetch_plagues()
  },
  methods: {
    fetch_plagues() {
      get_plages()
    },
    fetch_crop_data(id) {
      this.loading = true
      get_crop_details(id)
    },
    add_plague_to_crop() {
      add_plague_to_zone(this.id, this.new_plague.id, this.new_plague.date_reported)
      $('#modal-add-plaga').modal('hide')
    },
    control_plague() {
      control_plague_on_zone(this.id, this.plague_killed.id, this.plague_killed.date_finished)
      $('#modal-delete-plaga').modal('hide')
    },
    call_create_actv() {
      // create_active_prod('create_act_prod', this.new_activo.name, this.new_activo.date, this.new_activo.months, this.new_activo.curve, this.new_activo.sp_budget, this.new_activo.boxes, this.new_activo.est_cost)
      $('#modal-activo-productivo').modal('hide')
      this.$router.push({name: 'Crear Activo Productivo', params: {asset: this.new_activo, crop: this.id}})
    }
  }
}

const groove_details = {
  template: '#surco-details',
  props: ['groove_id', 'groove_name'],
  data: () => {
    return {
      message: 'Detalle de Surco',
      loading: false,
      error: false,
      groove: null,
      new_crop: {
        name: null,
        size: null,
        measure_unit: null,
        plant: null,
        density: null,
        crop_type: null,
        crop_type_variety: null
      },
      measure_units: null,
      plants: null,
      crop_types: null,
      crop_variety_types: null
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('groove_data_fetched', (data) => {
      this.groove = data
      this.loading = false

      if (Object.keys(this.groove).length == 0) {
        this.error = true;
      }
    })
    EventBus.$on('plant_list_fetched', (data) => {
      this.plants = data
    })
    EventBus.$on('crop_types_fetched', (data) => {
      this.crop_types = data
    })
    EventBus.$on('crop_variety_types_fetched', (data) => {
      this.crop_variety_types = data
    })
    EventBus.$on('no_empty_goove', (data) => {
      this.error = false
    })
    this.fetch_groove_data(this.groove_id)
    this.fetch_measure_units()
    this.call_plant_fetch_method()
    this.fetch_crop_types()
    this.fetch_crop_variety()
  },
  methods: {
    fetch_groove_data(id) {
      this.loading = true
      get_groove_details(id)
    },
    fetch_measure_units() {
      this.measure_units = JSON.parse(document.getElementById('measure-units').innerHTML)
    },
    call_plant_fetch_method() {
      EventBus.$emit('fetch_plants_signal')
    },
    fetch_crop_types() {
      get_crop_type()
    },
    fetch_crop_variety() {
      get_crop_type_variety()
    },
    send_create_crop() {
      create_crop(this.new_crop.name, this.new_crop.density, this.new_crop.plant, this.new_crop.crop_type, this.new_crop.crop_type_variety, this.new_crop.size, this.new_crop.measure_unit, this.groove_id)
      $('#modal-new-cultivo').modal('hide')
      this.clean_fields()
    },
    clean_fields() {
      for (var property in this.new_crop) {
        this.new_crop[property] = ''
      }
    }
  }
}

const tunnel_list = {
  template: '#tunnel-list-view',
  props: ['tunnels'],
  data: () => {
    return {
      message: 'Listado de túneles',
      loading: false,
      empty: false,
      error: false,
      tunel_list: null
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('tunnels_fetched', (data) => {
      this.tunel_list = data
      this.loading = false
    })
    this.fetch_tunnels()
  },
  methods: {
    fetch_tunnels() {
      get_all_tunnels()
      this.loading = true
    }
  }
}

const tunnel_details = {
  template: '#tunel-details',
  props: ['tunnel_id', 'tunnel_name'],
  data: () => {
    return {
      message: 'Detalle de Tunel',
      tunnel: null,
      loading: false,
      error: false,
      new_surco: {
        name: null,
        size: null,
        measure_unit: null
      },
      measure_units: null
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('tunnel_data_fetched', (data) => {
      this.tunnel = data;
      this.loading = false;

      if (Object.keys(this.tunnel).length == 0) {
        this.error = true;
      }
    })
    EventBus.$on('no_empty_tunnel', (data) => {
      this.error = false
    })
    this.fetch_tunel_details(this.tunnel_id)
    this.fetch_measure_units()
  },
  methods: {
    fetch_tunel_details(id) {
      this.loading = true
      get_tunel_data(id)
    },
    fetch_measure_units() {
      this.measure_units = JSON.parse(document.getElementById('measure-units').innerHTML)
    },
    send_create_groove() {
      create_groove(this.new_surco.name, this.new_surco.size, this.new_surco.measure_unit, this.tunnel_id)
      $('#modal-new-surco').modal('hide')
      this.clean_fields()
    },
    clean_fields() {
      this.new_surco.name = ''
      this.new_surco.size = ''
      this.new_surco.measure_unit = ''
    }
  }
}


const parcel_detail = {
  template: '#parcel-detail-template',
  props: ['parcel_id', 'parcel_name'],
  data: () => {
    return {
      message: 'Detalle de parcela',
      parcel: null,
      loading: false,
      error: false,
      plagues: null,
      new_tunnel_name: null,
      new_tunnel_size: null,
      new_measure_unit: null,
      measure_units: null,
      new_plague: {
        id: null,
        date_reported: null
      }
    }
  },
  created() {
    EventBus.$on('parcel_data_fetched', (data) => {
      this.parcel = data
      this.loading = false

      if (Object.keys(this.parcel).length == 0) {
        this.error = true
      }
    })
    EventBus.$on('no-empty-parcela', (data) => {
      this.error = false
    })
    // EventBus.$on('plagues_fetched', (data) => {
    //   this.plagues = data
    // })
    this.fetch_parcel_details(this.parcel_id)
    this.fetch_measure_units()
    // this.fetch_plagues()
  },
  methods: {
    fetch_parcel_details(id) {
      this.loading = true;
      EventBus.$emit('fetch_parcel_details', id)
    },
    fetch_measure_units() {
      this.measure_units = JSON.parse(document.getElementById('measure-units').innerHTML)
    },
    // fetch_plagues() {
    //   get_plages()
    // },
    send_create_tunnel() {
      //calling the create tunnel function outside vuejs
      create_tunnel(this.new_tunnel_name, this.new_tunnel_size, this.new_measure_unit, this.parcel_id)
      $('#modal-new-tunel').modal('hide');
      this.clean_fields()
    },
    clean_fields() {
      this.new_tunnel_name = ''
      this.new_tunnel_size = ''
      this.new_measure_unit = ''
    }
  }
}

const terrain_details = {
  template: '#terrain-detail',
  props: ['id', 'name'],
  data: () => {
    return {
      terrainmsg: 'This is the terrain detail view',
      loading: false,
      error: false,
      terrain: null,
      measure_units: null,
      unidades_parcela: null,
      new_parcel_name: null,
      new_parcel_unit: null,
      new_parcel_size: null,
      new_unit_selected: null,
      new_parcel_unit_selected: null
    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('terrain_details_show', (data) => {
      this.terrain = data
      this.loading = false

      if ( Object.keys(this.terrain).length == 0) {
        this.error = true
      }
    })
    EventBus.$on('load_parcel_unit', (data) => {
      this.unidades_parcela = data
    })
    EventBus.$on('load_measure_unit_on_terrain_details', (data) => {
      this.measure_units = data
    })
    EventBus.$on('no_empty_terrain', (data) => {
      this.error = false
    })
    // EventBus.$on('new_parcel_created', (data) => {
    //
    // })
    this.fetch_terrains_details()
    this.get_units_for_parcel()
  },
  methods: {
    fetch_terrains_details() {
      this.error = null
      this.loading = true
      EventBus.$emit('fetch_terrain_details', this.id)
    },
    get_units_for_parcel() {
      EventBus.$emit('get_units', this.id)
    },
    reset_parcel_modal() {
      this.new_parcel_name = ''
      this.new_parcel_size = ''
      this.new_unit_selected = ''
      this.new_parcel_unit_selected = '';
    },
    dispatch_new_parcel() {
      // let terrain = document.getElementById('terrain-parcel');
      // let terrain_param = terrain.getAttribute('data-terrain-id')
      create_parcel(this.new_parcel_name, this.new_parcel_size, this.new_unit_selected, this.id, this.new_parcel_unit_selected)
    }
  }
}

const terrains_component = {
  template: '#terrain-list-template',
  props: ['terrain_list'],
  data: () => {
    return {
      terrainsMsg: 'These are the Terrain List',
    }
  }
}

//the routes
const routes = [
  {
    path: '/',
    name: 'Terrenos',
    component: terrains_component,
    props: true
  },
  {
    path: '/terrain/:id',
    name: 'Detalle de Terreno',
    component: terrain_details,
    props: true
  },
  {
    path: '/parcel/:id',
    name: 'Detalle de Parcela',
    component: parcel_detail,
    props: true
  },
  {
    path: '/tunnels',
    name: 'Tuneles',
    component: tunnel_list,
    props: true
  },
  {
    path: 'tunnel/:id',
    name: 'tunnel_details',
    component: tunnel_details,
    props: true
  },
  {
    path: 'groove/:id',
    name: 'groove_details',
    component: groove_details,
    props: true
  },
  {
    path: 'crop/:id',
    name: 'Detalle de Cultivo',
    component: crop_details,
    props: true
  },
  {
    path: '/plants',
    name: 'Plantas',
    component: plants,
    props: true
  },
  {
    path: '/assets/create',
    name: 'Crear Activo Productivo',
    component: productive_asset,
    props: true
  }
]

const router = new VueRouter({
  routes
})

const EventBus = new Vue()

const App = new Vue({
  // el: '#farm-vue-area',
  data: {
    message: 'Agrisuite...',
    terrains: {},
    terrain_selected: null,
    tunnel_selected: null,
    crop_selected: null,
    measure_units: null,
    response_result: null,
    parcel_created: null,
    parcel_selected: null,
    parcel_unit: null,
    parcel_unit_response: null,
    groove_selected: null,
    tunnels: null,
    plant_list: null,
    crop_types: null,
    crop_variety_types: null,
    plagues: null,
    actv_id: null
  },
  methods: {
    init_terrains() {
      if(document.getElementById('farm-data')) {
        this.terrains = JSON.parse(document.getElementById('farm-data').innerHTML)
      }
    },
    get_response() {

    },
    get_created_terrain(){
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      // this.terrains[this.response_result.id] = {
      //   name: this.response_result.name
      // }
      // this.terrains = Object.assign({}, this.terrains, { a: 1, b: 2 })
      this.$set(this.terrains, this.response_result.id, {name: this.response_result.name})
    },
    get_created_parcel(parcel_unit, terrain_id) {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      // for (var terrain in this.terrain_selected) {
      //   this.$set(this.terrain_selected[terrain].parcelas, this.response_result.id, { name: this.response_result.name })
      // }
      if (Object.keys(this.terrain_selected).length == 0) {
        this.fetch_terrain_details(terrain_id)
        EventBus.$emit('no_empty_terrain', terrain_id)
      } else {
        this.fetch_terrain_details(terrain_id)
        // this.$set(this.terrain_selected[parcel_unit].parcelas, this.response_result.id, {name: this.response_result.name})
      }

    },
    get_created_tunnel(parcel_id) {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.fetch_parcel_details(parcel_id)
      EventBus.$emit('no-empty-parcela', this.parcel_selected)
    },
    get_created_groove(tunnel_id) {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.fetch_tunnel_details(tunnel_id)
      EventBus.$emit('no_empty_tunnel', tunnel_id)
    },
    get_created_crop(groove_id) {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.fetch_groove_data(groove_id)
      EventBus.$emit('no_empty_goove', groove_id)
    },
    get_created_plant() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.fetch_all_plants()
    },
    get_measure_units() {
      if(document.getElementById('measure-units')) {
        this.measure_units = JSON.parse(document.getElementById('measure-units').innerHTML)
      }
    },
    fetch_terrain_details(id) {
      get_terrain_details(id)
    },
    fetch_units_for_parcel() {
      get_units_info()
    },
    fetch_parcel_details(id) {
      get_parcel_details(id)
    },
    fetch_tunnel_details(id) {
      get_tunel_data(id)
    },
    fetch_all_plants() {
      get_plant_list()
    },
    fetch_groove_data(id) {
      get_groove_details(id)
    },
    fetch_crop_details(id){
      get_crop_details(id)
    },
    pass_parcel_units() {
      this.parcel_unit_response = JSON.parse(document.getElementById('response-result-units').innerHTML)
      this.parcel_unit = this.parcel_unit_response.results
      EventBus.$emit('load_parcel_unit', this.parcel_unit)
    },
    pass_parcel_details() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML);
      this.parcel_selected = this.response_result.results
      EventBus.$emit('parcel_data_fetched', this.parcel_selected)
    },
    update_terrain_data() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.terrain_selected = this.response_result.results
      EventBus.$emit('terrain_details_show', this.terrain_selected)
      EventBus.$emit('load_measure_unit_on_terrain_details', this.measure_units)
    },
    pass_tunnel_data() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.tunnel_selected = this.response_result.results
      EventBus.$emit('tunnel_data_fetched', this.tunnel_selected)
    },
    pass_groove_data() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.groove_selected = this.response_result.results
      EventBus.$emit('groove_data_fetched', this.groove_selected)
    },
    pass_tunnels_list() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.tunnels = this.response_result.results
      EventBus.$emit('tunnels_fetched', this.tunnels)
    },
    pass_plant_list() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.plant_list = this.response_result.results
      EventBus.$emit('plant_list_fetched', this.plant_list)
      EventBus.$emit('plan_list_populated', this.plant_list)
    },
    pass_crop_types() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.crop_types = this.response_result.results
      EventBus.$emit('crop_types_fetched', this.crop_types)
    },
    pass_crop_variety_types() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.crop_variety_types = this.response_result.results
      EventBus.$emit('crop_variety_types_fetched', this.crop_variety_types)
    },
    pass_plagues() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.plagues = this.response_result.results
      EventBus.$emit('plagues_fetched', this.plagues)
    },
    plague_to_crop(crop_id) {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.fetch_crop_details(crop_id)
    },
    pass_crop_data() {
      this.response_result = JSON.parse(document.getElementById('response_result').innerHTML)
      this.crop_selected = this.response_result.results
      EventBus.$emit('crop_details_fetched', this.crop_selected)
    },
    create_actv(data) {
      let url = document.getElementById('response_result').textContent
      alert(url)
      EventBus.$emit('url_obtained', url)

    }
  },
  created() {
    //do something after creating vue instance
    EventBus.$on('fetch_terrain_details', (data) => {
      this.fetch_terrain_details(data)
    })
    EventBus.$on('get_units', (data) => {
      this.fetch_units_for_parcel()
    })
    EventBus.$on('fetch_parcel_details', (data) => {
      this.fetch_parcel_details(data)
    })
    EventBus.$on('fetch_plants_signal', (data) => {
      this.fetch_all_plants()
    })

    this.init_terrains()
    this.get_measure_units()
  },
  router
}).$mount('#farm-vue-area')
